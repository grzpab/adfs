/*
"adfs_fs.c" written by GrzPab (grzpab.eu5.org) as a part of the "Designing a File System" course.
This piece of code, written in the C language, is distributed with absolutely no warranty. Should any damage or loss happen to any software or hardware products by using the knowledge included in this code or the code itself, the author will have no liability towards any person, entitle, company or government which has used the code.
All rights reserved. You may redistribute this software freely as long as you credit GrzPab for his work and provide a link to this website (grzpab.eu5.org).
*/

#include "adfs_lib.h"

//adfs_fs private functions:

uint8_t adfs_fs_close(adfs_fs* fs)
{
	if(fs == NULL || fs->file == -1) return ADFS_FAILURE;

	///close all open directories and files;
	adfs_list_close(&(fs->opened_dirs_head), &(fs->opened_dirs_tail));
	adfs_list_close(&(fs->opened_files_head), &(fs->opened_files_tail));

	///update the filesystem's superblock:
	uint8_t success = ADFS_FAILURE;

	if(lseek(fs->file, 0, SEEK_SET) == 0 && write(fs->file, &(fs->superblock), sizeof(fs->superblock)) == sizeof(fs->superblock))
		success = ADFS_SUCCESS;

	///close the file
	close(fs->file);
	fs->file = -1; //just in case

	free(fs->clusters_bitmap);
	free(fs->inodes_bitmap);
	free(fs->bnodes_bitmap);
	free(fs);

	return success;
}

adfs_fs* adfs_fs_create(char* filepath, char* name, uint64_t size)
{
	if(filepath == NULL || name == NULL || size < 1024*1024) return NULL; //at least 1 MiB partition

	int file;
	if((file = open(filepath, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH)) == -1) return NULL;

	adfs_fs* fs;
	if((fs = malloc(sizeof(adfs_fs))) == NULL) return NULL;

	uint8_t failure = 0;

	///the superblock
	memcpy(fs->superblock.magic_number_1, ADFS_SUPERBLOCK_MAGIC_NUMBER_1, 8); //the first magic number
	strncpy(fs->superblock.name, name, 24); //the volume name

	size &= ~(ADFS_CLUSTER_SIZE - 1); // < original size (this trick works only if ADFS_CLUSTER_SIZE is a power of two)
	fs->superblock.volume_size = size; //proper size of the volume

	///b-trees = 1/32 or less
	uint64_t primary_block = (size / 32) & ~(ADFS_CLUSTER_SIZE - 1); //dividable by ADFS_CLUSTER_SIZE
	uint64_t bitmap_block = primary_block / 8 / ADFS_CLUSTER_SIZE;
	bitmap_block = bitmap_block < ADFS_CLUSTER_SIZE ? ADFS_CLUSTER_SIZE : ((bitmap_block & ~(ADFS_CLUSTER_SIZE - 1)) + ADFS_CLUSTER_SIZE);

	fs->superblock.bnodes_bitmap = ADFS_CLUSTER_SIZE;
	fs->superblock.bnodes_data = ADFS_CLUSTER_SIZE + bitmap_block;

	///inodes = 1/32 or less
	//primary blocks stays the same
	bitmap_block = primary_block / 8 / 128;
	bitmap_block = bitmap_block < ADFS_CLUSTER_SIZE ? ADFS_CLUSTER_SIZE : ((bitmap_block & ~(ADFS_CLUSTER_SIZE - 1)) + ADFS_CLUSTER_SIZE);

	fs->superblock.inodes_bitmap = ADFS_CLUSTER_SIZE + primary_block;
	fs->superblock.inodes_data = ADFS_CLUSTER_SIZE + primary_block + bitmap_block;

	///clusters = 15/16 or more
	fs->superblock.clusters_bitmap = ADFS_CLUSTER_SIZE + 2*primary_block;
	//the primary block is now different
	primary_block = size - ADFS_CLUSTER_SIZE - 2*primary_block;
	bitmap_block = bitmap_block < ADFS_CLUSTER_SIZE ? ADFS_CLUSTER_SIZE : ((bitmap_block & ~(ADFS_CLUSTER_SIZE - 1)) + ADFS_CLUSTER_SIZE);
	fs->superblock.clusters_data = fs->superblock.clusters_bitmap + bitmap_block;
	fs->superblock.blocks_available = primary_block / ADFS_CLUSTER_SIZE;
	fs->superblock.blocks_used = 0;

	///files and directories
	fs->superblock.number_of_files = 0;
	fs->superblock.number_of_dirs = 1; //the root directory
	fs->superblock.root_dir_id = 0; //the first one possible (obvious)

	///write the superblock and enforce the volume's size
	if(write(file, &(fs->superblock), sizeof(struct adfs_superblock)) != sizeof(struct adfs_superblock)) failure = 1; //save the superblock

	if((uint64_t)lseek(file, size-1, SEEK_SET) == (size-1)) //the simplest solution to this problem
	{
		unsigned char x = 0;
		if(write(file, &x, 1) != 1) failure = 1;
	}
	else failure = 1;

	///set appropriate bitmap size for these 3 bitmaps
	fs->bnodes_bitmap_size = fs->superblock.bnodes_data - fs->superblock.bnodes_bitmap;
	fs->inodes_bitmap_size = fs->superblock.inodes_data - fs->superblock.inodes_bitmap;
	fs->clusters_bitmap_size = fs->superblock.clusters_data - fs->superblock.clusters_bitmap;

	///allocate memory for the bitmaps
	fs->bnodes_bitmap = (unsigned char*) malloc(fs->bnodes_bitmap_size);
	fs->inodes_bitmap = (unsigned char*) malloc(fs->inodes_bitmap_size);
	fs->clusters_bitmap = (unsigned char*) malloc(fs->clusters_bitmap_size);

	///check if the memory has been really allocated
	if(fs->bnodes_bitmap == NULL || fs->inodes_bitmap == NULL || fs->clusters_bitmap == NULL)
	{
		//the file system's file has been created, we should just close it
		close(file);

		free(fs->clusters_bitmap);
		free(fs->inodes_bitmap);
		free(fs->bnodes_bitmap);
		free(fs);

		return NULL;
	}

	///fill these bitmaps with zeros (no data yet)
	memset(fs->bnodes_bitmap, 0, fs->superblock.bnodes_data - fs->superblock.bnodes_bitmap);
	memset(fs->inodes_bitmap, 0, fs->superblock.inodes_data - fs->superblock.inodes_bitmap);
	memset(fs->clusters_bitmap, 0, fs->superblock.clusters_data - fs->superblock.clusters_bitmap);

	///adfs_fs structure's additional fields
	fs->opened_dirs_head = NULL;
	fs->opened_dirs_tail = NULL;
	fs->opened_files_head = NULL;
	fs->opened_files_tail = NULL;
	fs->file = file;

	///create the root directory's b-tree node
	fs->bnodes_bitmap[0] = 0x80; //the highest bit
	fs->inodes_bitmap[0] = 0x80; //again
	fs->clusters_bitmap[0] = 0x80; //and yet again

	///reserve the highest bit (bnodes' bitmap)
	if((uint64_t)lseek(fs->file, fs->superblock.bnodes_bitmap, SEEK_SET) == fs->superblock.bnodes_bitmap)
	{
		if(write(fs->file, fs->bnodes_bitmap, 1) != 1) failure = 1;
	}
	else failure = 1;

	///inodes' bitmap
	if((uint64_t)lseek(fs->file, fs->superblock.inodes_bitmap, SEEK_SET) == fs->superblock.inodes_bitmap)
	{
		if(write(fs->file, fs->inodes_bitmap, 1) != 1) failure = 1;
	}
	else failure = 1;

	///clusters' bitmaps
	if((uint64_t)lseek(fs->file, fs->superblock.clusters_bitmap, SEEK_SET) == fs->superblock.clusters_bitmap)
	{
		if(write(fs->file, fs->clusters_bitmap, 1) != 1) failure = 1;
	}
	else failure = 1;

	///prepare a node (we cannot use the adfs_bnode_prepare function)
	adfs_bnode rd_node; //root directory
	memset(&rd_node, 0, sizeof(adfs_bnode));
	rd_node.is_a_leaf = 1;

	///save the node
	if((uint64_t)lseek(fs->file, fs->superblock.bnodes_data, SEEK_SET) == fs->superblock.bnodes_data)
	{
		if(write(fs->file, &rd_node, sizeof(adfs_bnode)) != sizeof(adfs_bnode)) failure = 1;
	}
	else failure = 1;

	/// return the pointer to the file system's structure
	if(failure == 1)
	{
		//it this "free" list complete?
		free(fs->clusters_bitmap);
		free(fs->inodes_bitmap);
		free(fs->bnodes_bitmap);
		free(fs);
		return NULL;
	}

	return fs;
}

adfs_fs* adfs_fs_open(char* filepath)
{
	if(filepath == NULL) return NULL;

	///check whether it is a valid file or not
	int file;
	if((file = open(filepath, O_RDWR)) == -1) return NULL;

	adfs_fs* fs;
	if((fs = (adfs_fs*) malloc(sizeof(adfs_fs))) == NULL) return NULL;

	if(	(read(file, &(fs->superblock), sizeof(struct adfs_superblock)) != sizeof(struct adfs_superblock)) || (memcmp(fs->superblock.magic_number_1, ADFS_SUPERBLOCK_MAGIC_NUMBER_1, 8) != 0) )
	{
		//the superblock couldn't be readed or it is not a superblock
		free(fs);
		return NULL;
	}

	///set appropriate bitmap size for these 3 bitmaps
	fs->bnodes_bitmap_size = fs->superblock.bnodes_data - fs->superblock.bnodes_bitmap;
	fs->inodes_bitmap_size = fs->superblock.inodes_data - fs->superblock.inodes_bitmap;
	fs->clusters_bitmap_size = fs->superblock.clusters_data - fs->superblock.clusters_bitmap;

	///allocate memory for the bitmaps
	fs->bnodes_bitmap = (unsigned char*) malloc(fs->bnodes_bitmap_size);
	fs->inodes_bitmap = (unsigned char*) malloc(fs->inodes_bitmap_size);
	fs->clusters_bitmap = (unsigned char*) malloc(fs->clusters_bitmap_size);

	///check the allocation and read the data (the proper order guaranteed)
	if(fs->bnodes_bitmap == NULL || fs->inodes_bitmap == NULL || fs->clusters_bitmap == NULL || (uint64_t)read(file, fs->bnodes_bitmap, fs->bnodes_bitmap_size) != fs->bnodes_bitmap_size || (uint64_t)read(file, fs->inodes_bitmap, fs->inodes_bitmap_size) != fs->inodes_bitmap_size || (uint64_t)read(file, fs->clusters_bitmap, fs->clusters_bitmap_size) != fs->clusters_bitmap_size)
	{
		free(fs->clusters_bitmap);
		free(fs->inodes_bitmap);
		free(fs->bnodes_bitmap);

		free(fs);
		return NULL;
	}

	fs->file = file;

	return fs;
}

uint8_t adfs_list_close(adfs_list** head, adfs_list** tail)
{
	if(head == NULL || *head == NULL) return ADFS_FAILURE;

	adfs_list *to_remove1 = *head, *to_remove2 = (*head)->next;

	for(;;)
	{
		free(to_remove1);
        if(to_remove2 == NULL) break;

        to_remove1 = to_remove2;
        to_remove2 = to_remove2->next;
	}

	*head = NULL;
	if(tail != NULL) *tail = NULL;

	return ADFS_SUCCESS;
}

uint8_t adfs_list_insert(adfs_list** head, adfs_list** tail, void* element)
{
	if(head == NULL || tail == NULL || element == NULL) return ADFS_FAILURE;

	adfs_list* new_tag = malloc(sizeof(adfs_list));
	if(new_tag == NULL) return ADFS_FAILURE;

	new_tag->element = element;
	new_tag->next = NULL;

	if(*head == NULL) //actually, no list
	{
		*head = new_tag;
		*tail = new_tag;
		return ADFS_SUCCESS;
	}

	(*tail)->next = new_tag;
	*tail = new_tag;

	return ADFS_SUCCESS;
}

uint8_t adfs_list_remove(adfs_list** head, adfs_list** tail, void* element)
{
	if(head == NULL || tail == NULL || element == NULL || *head == NULL) return ADFS_FAILURE;

	if((*head)->element == element)
	{
		adfs_list* new_head = (*head)->next;
		free(*head);
		*head = new_head;

		if(new_head == NULL) *tail = NULL;
		else if(new_head->next == NULL) *tail = new_head;

		return ADFS_SUCCESS;
	}

	adfs_list* previous = *head;
	uint8_t found = 0;

	while(previous->next != NULL)
	{
		if(previous->next->element == element)
		{
			found = 1;
			break;
		}
		else previous = previous->next;
	}

	if(found == 0) return ADFS_FAILURE; //not found

	adfs_list* to_remove = previous->next; //!= NULL
	previous->next = to_remove->next;

	free(to_remove);

	if(previous->next == NULL) *tail = previous;

	return ADFS_SUCCESS;
}

uint64_t adfs_number_of_dirs(adfs_fs* fs)
{
	if(fs == NULL) return 0; //no file system, no directories
	return fs->superblock.number_of_dirs;
}

uint64_t adfs_number_of_files(adfs_fs* fs)
{
	if(fs == NULL) return 0; //no file system, no files
	return fs->superblock.number_of_files;
}

char* adfs_path_last(const char* path)
{
	#ifdef ADFS_DEBUG
	if(path == NULL) return NULL;
	#endif

	uint8_t end = 0;
	size_t increment = 0;

	char* name = malloc(strlen(path)+1);
	if(name == NULL) return NULL;

	char* original_name = name; //pointing to the very beginning of the path
	strcpy(name, path);

	while(end == 0)
	{
		name += increment;
		name = adfs_path_next(name, &end, &increment);
	}

	char* result = malloc(strlen(name)+1);
	if(result == NULL)
	{
		free(original_name);
		return NULL;
	}

	strcpy(result, name);
	free(original_name);

	return result;
}

char* adfs_path_next(char* path, uint8_t* end, size_t* increment)
{
	#ifdef ADFS_DEBUG
	if(path == NULL || end == NULL || increment == NULL) return NULL;
	#endif

	size_t length = strlen(path);
	if(length == 0) return NULL;

	size_t i = 0;
	uint8_t sequence = 0; //no sequence
	size_t start = 0;

	for(; i < length; i++)
	{
		if(path[i] == '/')
		{
			if(sequence == 0) start++;
			else //sequence == 1
			{
				path[i] = 0;
				*end = 0;
				*increment = i - start + 1;
				return (path + start); //the end of the sequence
			}

		}
		else if(sequence == 0) sequence = 1;
	}

	*end = 1;
	if(sequence == 0)
	{
		*increment = 0;
		return NULL;
	}

	*increment = i - start + 1;
	return (path + start);
}
