/*
"adfs_cluster.c" written by GrzPab (grzpab.eu5.org) as a part of the "Designing a File System" course.
This piece of code, written in the C language, is distributed with absolutely no warranty. Should any damage or loss happen to any software or hardware products by using the knowledge included in this code or the code itself, the author will have no liability towards any person, entitle, company or government which has used the code.
All rights reserved. You may redistribute this software freely as long as you credit GrzPab for his work and provide a link to this website (grzpab.eu5.org).
*/

#include "adfs_lib.h"

//adfs_cluster private functions:

uint8_t adfs_cluster_fill(adfs_fs* fs, uint32_t cluster_id)
{
	///fills the cluster with zeroes (or zeros)
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || cluster_id == 0) return ADFS_FAILURE;
	#endif

	off_t offset = fs->superblock.clusters_data + cluster_id*ADFS_CLUSTER_SIZE;
	if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;

	unsigned char buffer [ADFS_CLUSTER_SIZE] = {0};
	if(write(fs->file, buffer, ADFS_CLUSTER_SIZE) != ADFS_CLUSTER_SIZE) return ADFS_FAILURE;

	return ADFS_SUCCESS;
}

uint8_t adfs_cluster_read(adfs_fs* fs, uint32_t cluster_id, uint16_t cluster_pos, void* buffer, uint16_t buffer_count)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || cluster_id == 0 || cluster_pos >= ADFS_CLUSTER_SIZE || buffer == NULL || buffer_count > ADFS_CLUSTER_SIZE) return ADFS_FAILURE;
	#endif

	off_t offset = fs->superblock.clusters_data + cluster_id*ADFS_CLUSTER_SIZE + cluster_pos;
	if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;
	if((uint16_t)(read(fs->file, buffer, buffer_count)) != buffer_count) return ADFS_FAILURE;

	return ADFS_SUCCESS;
}

uint8_t	adfs_cluster_release(adfs_fs* fs, uint32_t cluster_id, uint16_t length)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || cluster_id == 0 || length == 0) return ADFS_FAILURE;
	#endif

	///position in bytes
	uint64_t position = cluster_id / 8;
	off_t offset = fs->superblock.clusters_bitmap + position;
	if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;

	///position in bits (for bit shifts)
	size_t j = 7 - (cluster_id % 8);

	///loop
	for(; length != 0; length--, j--)
	{
		fs->clusters_bitmap[position] &= (~(1 << j));

		if(j == 0)
		{
			if(write(fs->file, fs->clusters_bitmap + position, 1) != 1) return ADFS_FAILURE;

			j = 8; //will be soon decremented (to 7)
			position++; //next byte
		}
	}
	//last byte
	if(j != 0)
	{
		if(write(fs->file, fs->clusters_bitmap + position, 1) != 1) return ADFS_FAILURE;
	}

	fs->superblock.blocks_used -= length;

	return ADFS_SUCCESS;
}

uint32_t adfs_cluster_request(adfs_fs* fs, uint16_t length)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || length == 0) return 0;
	#endif

	uint64_t position = 0; //actual byte
	uint32_t p = 0; //actual cluster id

	uint32_t start = 0;
	uint16_t size = 0;

	for(; position < fs->clusters_bitmap_size; position++)
	{
		int j;
		for(j = 7; j >= 0; j--)
		{
			if((fs->clusters_bitmap[position] & (1 << j)) != 0)
			{//the bit is set as '1'
				if(start != 0)
				{//we failed to find enough space
					start = 0;
					size = 0;
				}
			}
			else
			{//the bit is set as '0'
				if(start == 0)
				{
					start = p;
					size = 1;
				}
				else size++;

				if(size == length)
				{
					//set the proper bits
					off_t relative_offset = start/8;
					size_t k = (start % 8);

					off_t offset = fs->superblock.clusters_bitmap + relative_offset;
					if(lseek(fs->file, offset, SEEK_SET) != offset) return 0;

					for(size = 0; size < length;)
					{
						for(; k < 8 && size < length; k++, size++)
						{
							fs->clusters_bitmap[relative_offset] |= (1 << (7-k)); //bit is set
						}
						k = 0;

						if(write(fs->file, fs->clusters_bitmap + relative_offset, 1) != 1) return 0;
						relative_offset++;
					}

					return start;
				}
			}

			p++;
		}
	}

	return 0; //no space found
}

uint8_t adfs_cluster_write(adfs_fs* fs, uint32_t cluster_id, uint16_t cluster_pos, const void* buffer, uint16_t buffer_count)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || cluster_id == 0 || cluster_pos >= ADFS_CLUSTER_SIZE || buffer == NULL || buffer_count > ADFS_CLUSTER_SIZE) return ADFS_FAILURE;
	//cluster_pos: 0...2047, buffer_count: 0? , 1, ... 2048
	#endif

	off_t offset = fs->superblock.clusters_data + cluster_id*ADFS_CLUSTER_SIZE + cluster_pos;
	if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;
	if((uint16_t)(write(fs->file, buffer, buffer_count)) != buffer_count) return ADFS_FAILURE;

	return ADFS_SUCCESS;
}
