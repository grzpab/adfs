#ifndef ADFS_0_3_H
#define ADFS_0_3_H

#define ADFS_VERSION_0_3
#define ADFS_SUCCESS		1
#define ADFS_FAILURE		0

#define _FILE_OFFSET_BITS	64
#include <stdint.h>
#include <stddef.h>

typedef struct adfs_fs adfs_fs;
typedef struct adfs_file adfs_file;
typedef struct adfs_dir adfs_dir;

/* file system */
adfs_fs		*adfs_fs_create(char *, char *, uint64_t);
adfs_fs		*adfs_fs_open(char *);
uint8_t		adfs_fs_close(adfs_fs *);
uint64_t	adfs_number_of_files(adfs_fs *);
uint64_t	adfs_number_of_dirs(adfs_fs *);

/* regular files */
uint8_t		adfs_file_create(adfs_fs *, const char *);
adfs_file	*adfs_file_open(adfs_fs *, const char *);
uint32_t	adfs_file_size(adfs_fs *, const char *);
uint32_t	adfs_file_read(adfs_file *, void *, size_t);
uint32_t	adfs_file_write(adfs_file *, const void *, size_t);
uint32_t	adfs_file_seek(adfs_file *, uint32_t);
uint8_t		adfs_file_close(adfs_file *);
uint8_t		adfs_file_rename(adfs_fs *, const char *, const char *);
uint8_t		adfs_file_delete(adfs_fs *, const char *);
uint8_t		adfs_file_exists(adfs_fs *, const char *);

/* regular directories */
uint8_t		adfs_dir_create(adfs_fs *, const char *);
adfs_dir	*adfs_dir_open(adfs_fs *, const char *);
char		*adfs_dir_read(adfs_dir *);
uint8_t		adfs_dir_rewind(adfs_dir *);
uint32_t	adfs_dir_filenumber(adfs_dir *);
uint32_t	adfs_dir_size(adfs_fs *, const char *);
uint8_t		adfs_dir_close(adfs_dir *);
uint8_t		adfs_dir_rename(adfs_fs *, const char *, const char *);
uint8_t		adfs_dir_delete(adfs_fs *, const char *);

#endif
