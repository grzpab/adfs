#include "adfs_lib.h"
#include <stdio.h>

#define SUCCESS(x) printf("Test " #x " has succeeded.\n")
#define FAILURE(x) printf("Test " #x " has failed.\n")

int main()
{
	///TEST_0: creating a blank file system
	#define TEST_0
	#ifdef	TEST_0
	adfs_fs *fs = adfs_fs_create("fs", "ADFS 3", 64*1024*1024+100);
	if(fs == NULL) FAILURE(0);
	else SUCCESS(0);
	#endif

	///TEST_1: creating nested directories
	#define	TEST_1
	#ifdef	TEST_0
	uint8_t success = 0;
	char name [] = "/home/user/a";

	success += adfs_dir_create(fs, "/home");
	success += adfs_dir_create(fs, "/home/user");

	for(; name[sizeof(name)-2] <= 'z'; name[sizeof(name)-2]++)
	{
		success += adfs_dir_create(fs, name);
	}

	if(success == adfs_number_of_dirs(fs) - 1) SUCCESS(1); //the root directory does not count
	else FAILURE(1);
	#endif

	///TEST_2: listing files inside a directory
	#define	TEST_2
	#ifdef	TEST_2
	adfs_dir* dir = adfs_dir_open(fs, "/home/user");
	uint8_t status = 0;

	if(dir == NULL) FAILURE(2);
	else
	{
		int i = 0;
		for(; i < 2; i++)
		{
			char* name = NULL;
			char goodname [] = "a";

			while(status == 0 && ((name = adfs_dir_read(dir)) != NULL)) //the order is important (memory leaks)
			{
				if(strcmp(goodname, name) != 0) status = 1;
				free(name);
				goodname[0]++;
			}

			status += adfs_dir_rewind(dir);
			goodname[0] = 'a';
		}

		status += adfs_dir_close(dir);
	}

	if(status == 3) SUCCESS(2); //2x adfs_dir_rewind + adfs_dir_close
	else FAILURE(2);
	#endif

	///TEST_3: checking whether those directories exist
	#define	TEST_3
	#ifdef	TEST_3
	uint8_t exists = 0;

	exists += adfs_file_exists(fs, "/home");//+1
	exists += adfs_file_exists(fs, "/home/user");//+1
	exists += adfs_file_exists(fs, "/home/home");//+0
	exists += adfs_file_exists(fs, "/home/user/z");//+1
	exists += adfs_file_exists(fs, "/");//+1

	if(exists == 4) SUCCESS(3);
	else FAILURE(3);
	#endif

	///TEST_4: listing files within the root directory
	#define	TEST_4
	#ifdef	TEST_4
	adfs_dir* rootdir = adfs_dir_open(fs, "/");
	if(dir == NULL) FAILURE(4);
	else
	{
		char* name = adfs_dir_read(rootdir);
		uint8_t status = 0;

		if(name != NULL)
		{
			status++;
			if(strcmp(name, "home") == 0) status++;
			free(name);
		}

		if((name = adfs_dir_read(rootdir)) == NULL) status++;
		else free(name);

		status += adfs_dir_close(rootdir);

		if(status == 4) SUCCESS(4);
		else FAILURE(4);
	}
	#endif

	///TEST_5: looking for the number of files within some directories (two methods)
	#define TEST_5
	#ifdef	TEST_5

	//1st method
	adfs_dir* t5dir [] = {adfs_dir_open(fs, "/"), adfs_dir_open(fs, "/home"), adfs_dir_open(fs, "/home/user"), adfs_dir_open(fs, "/home/user/a")};
	uint8_t t5status = 0;

	uint8_t i = 0;
	uint32_t t5value = 1;
	for(; i < 4; i++)
	{
		t5value = (i < 2) ? 1 : (26*(3-i));

		if(t5dir[i])
		{
			if(adfs_dir_filenumber(t5dir[i]) == t5value) t5status++;
			if(adfs_dir_close(t5dir[i]) == 1) t5status++;
		}
	}

	//2nd method
	if(adfs_dir_size(fs, "/") == 1) t5status++;
	if(adfs_dir_size(fs, "/home") == 1) t5status++;
	if(adfs_dir_size(fs, "/home/user") == 26) t5status++;
	if(adfs_dir_size(fs, "/home/user/b") == 0) t5status++;

	if(t5status == 12) SUCCESS(5);
	else FAILURE(5);
	#endif

	///TEST_6: creating some files
	#define	TEST_6
	#ifdef	TEST_6

	uint8_t t6status = 0;
	char t6fname [] = "/home/user/a/file0";

	for(; t6fname[sizeof(t6fname)-2] <= '9'; t6fname[sizeof(t6fname)-2]++) t6status += adfs_file_create(fs, t6fname);

	if(t6status != 10) FAILURE(6);
	else
	{
		adfs_dir* t6dir = adfs_dir_open(fs, "/home/user/a");

		if(t6dir == NULL || adfs_dir_filenumber(t6dir) != 10) FAILURE(6);
		else if(adfs_dir_close(t6dir) == 1)	SUCCESS(6);
	}
	#endif

	///TEST_7: opening, writing to and reading from files
	#define	TEST_7
	#ifdef	TEST_7

	adfs_file* file = adfs_file_open(fs, "/home/user/a/file0");
	if(file == NULL) FAILURE(7);
	else
	{
		unsigned char to_write[20480], to_read [20480];
		uint32_t t7 = 0;

		int y = 0;
		int j  = 1;
		for(; j < 11; j++)
		{
			for(; y < 2048*j; y++) to_write[y] = j*0x10;
		}

		t7 += adfs_file_write(file, to_write, 20480); //+20480
		if(adfs_file_seek(file, 0) == 0) t7++; //+1
		t7 += adfs_file_read(file, to_read, 20480); //+20480

		for(y = 0; y < 20480; y++)
		{
			if(to_write[y] == to_read[y]) t7++;
		} //+20480

		if(file->offset == 20480) t7++; //+1
		t7 += adfs_file_close(file); //+1

		if(t7 == 3*20480+3) SUCCESS(7);
		else FAILURE(7);
	}
	#endif

	///TEST_8: renaming files and directories and removing the home directory;
	uint8_t t8 = 0;

	t8 += adfs_file_rename(fs, "/home/user/a/file0", "/home/file"); //+1
	t8 += adfs_file_exists(fs, "/home/user/a/file0"); //+0
	t8 += adfs_file_exists(fs, "/home/file"); //+1

	t8 += adfs_dir_rename(fs, "/home/user", "/home/user2"); //+1
	t8 += adfs_file_exists(fs, "/home/user"); //+0
	t8 += adfs_file_exists(fs, "/home/user2"); //+1

	t8 += adfs_dir_delete(fs, "/home"); //+1
	t8 += adfs_file_exists(fs, "/home"); //+0

	if(t8 == 5) SUCCESS(8);
	else FAILURE(8);

	///TEST_9: closing the file system
	#define TEST_9
	#ifdef	TEST_9
	if(adfs_fs_close(fs) == 1) SUCCESS(9);
	else FAILURE(9);
	#endif

	return 0;
}
