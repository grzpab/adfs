/*
"adfs_bnode.c" written by GrzPab (grzpab.eu5.org) as a part of the
"Designing a File System" course.
This piece of code, written in the C language, is distributed with absolutely
no warranty. Should any damage or loss happen to any software or
hardware products by using the knowledge included in this code or the code
itself, the author will have no liability towards any person, entitle, company
or government which has used the code.
All rights reserved. You may redistribute this software freely as long as you
credit GrzPab for his work and provide a link to this website (grzpab.eu5.org).

Great parts of this file were written using the articles provided by
the GeeksforGeeks website: (THANK YOU!)
a) http://www.geeksforgeeks.org/b-tree-set-1-introduction-2/
b) http://www.geeksforgeeks.org/b-tree-set-1-insert-2/
c) http://www.geeksforgeeks.org/b-tree-set-3delete/
I also used the book "Introduction to Algorithms" by Thomas H. Cormen,
Charles E. Leiserson, Ronald L. Rivest and Clifford Stein.
Hereby I do not claim that those parts of the following piece of code, treating
with the issues concerning the B-tree algorithm, belong to my intelectual
property.
*/
#include "adfs_lib.h"

//adfs_bnode private functions

uint8_t adfs_bnode_add (adfs_fs*     fs,
                        uint32_t     r_id,
                        adfs_bnode  *r,
                        const char  *filename,
                        uint32_t     rr_id,
                        uint8_t      entry_type)
{
        #ifdef ADFS_DEBUG
        if (fs == NULL || fs->file == -1 || r_id > (fs->bnodes_bitmap_size*8-1)
            || r == NULL || filename == NULL ||
            rr_id > (fs->bnodes_bitmap_size*8-1) || entry_type > 1) {
                return ADFS_FAILURE;
        }
        #endif
        ///r = root, rr = the root of the root

        if (r->size == (2*ADFS_BNODE_ORDER-1))
        {
                ///prepare the "to-be-split" node
                adfs_bnode* split = NULL;
                //is_a_leaf = 1 [MALLOC]
                uint32_t split_id = adfs_bnode_prepare(fs, &split);

                if(split_id == 0 || split == NULL) {
                      return ADFS_FAILURE;
                }

                ///change the root - something like "r_id = split_id"
                if (!adfs_bnode_rtchange(fs, r_id, rr_id, split_id)) {
                        return ADFS_FAILURE;
                }

                split->is_a_leaf = 0;
                split->pointer[0] = r_id;

                if (!adfs_bnode_split(fs, split_id, split, r_id, r, 0)) {
                        return ADFS_FAILURE;
                }
                if (!adfs_bnode_insert(fs, split_id, split, filename,
                                       entry_type)) {
                        return ADFS_FAILURE;
                }

                free(split);

                return ADFS_SUCCESS;
        }

        if (!adfs_bnode_insert(fs, r_id, r, filename, entry_type)) {
                return ADFS_FAILURE;
        }
        return ADFS_SUCCESS;
}

uint32_t adfs_bnode_create (adfs_fs* fs)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1) {
                return 0;
        }
        #endif

        uint32_t id = adfs_bnode_request(fs);
        if (id == 0) {
                return 0;
        }

        adfs_bnode node;
        memset(&node, 0, sizeof(adfs_bnode));
        node.is_a_leaf = 1;
        //node.size = 0; //thanks to memset

        if (!adfs_bnode_write(fs, id, &node)) {
                return 0;
        }

        return id;
}

uint8_t adfs_bnode_delete (adfs_fs* fs,
                           uint32_t r_id,
                           adfs_bnode* r,
                           const char* filename,
                           uint32_t rr_id)
{
        #ifdef ADFS_DEBUG
        if (fs == NULL || fs->file == -1 || r_id > (fs->bnodes_bitmap_size*8-1)
            || r == NULL || filename == NULL ||
            rr_id > (fs->bnodes_bitmap_size*8-1)) {
                return ADFS_FAILURE;
        }
        #endif

        if(!adfs_bnode_remove(fs, r_id, r, filename)) {
                return ADFS_FAILURE;
        }

        if(r->size == 0){
                if(r->is_a_leaf == 0){
                        if(!adfs_bnode_rtchange(fs, r_id, rr_id, r->pointer[0])) {
                                return ADFS_FAILURE;
                        }
                }
        }

        return ADFS_SUCCESS;
}

uint8_t adfs_bnode_fill(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, uint8_t i)
{
        #ifdef ADFS_DEBUG
        if (fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size-1)
            || x == NULL || i > (2*ADFS_BNODE_ORDER-1)) {
                return ADFS_FAILURE;
        }
        #endif

        if (i != 0) {
                adfs_bnode* sibling = adfs_bnode_read(fs, x->pointer[i-1]);
                if (sibling == NULL) {
                        return ADFS_FAILURE;
                }

                if (sibling->size >= ADFS_BNODE_ORDER) {
                    adfs_bnode* child = adfs_bnode_read(fs, x->pointer[i]);
                    if(child == NULL) {
                            free(sibling);
                            return ADFS_FAILURE;
                    }

                uint8_t j;
                for(j = child->size - 1; ; j--)
                {
                        adfs_bnode_key_rep(child, j+1, child, j);
                                if(j == 0) break;
                }

                    if(child->is_a_leaf == 0)
                    {
                            for(j = child->size; /*j >= 0*/; j--)
                            {
                                    child->pointer[j+1] = child->pointer[j];
                                    if(j == 0) break;
                            }
                    }

                        adfs_bnode_key_rep(child, 0, x, i-1);

                        if(x->is_a_leaf == 0) child->pointer[0] = sibling->pointer[sibling->size];

                        adfs_bnode_key_rep(x, i-1, sibling, sibling->size-1);

                        child->size++;
                        sibling->size--;

                        ///write and free allocated memory
                        if(adfs_bnode_write(fs, x->pointer[i], child) == ADFS_FAILURE || adfs_bnode_write(fs, x->pointer[i-1], sibling) == ADFS_FAILURE || adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE)
                        {
                                free(child);
                                free(sibling);
                                return ADFS_FAILURE;
                        }
                        free(child);

                }
                free(sibling);
        }
        else if(i != x->size)
        {
                adfs_bnode* sibling = adfs_bnode_read(fs, x->pointer[i+1]);
                if(sibling == NULL) return ADFS_FAILURE;

                if(sibling->size >= ADFS_BNODE_ORDER)
                {
                        adfs_bnode* child = adfs_bnode_read(fs, x->pointer[i]);
                        if(child == NULL)
                        {
                                free(sibling);
                                return ADFS_FAILURE;
                        }

                        adfs_bnode_key_rep(child, child->size, x, i);

                        if(child->is_a_leaf == 0) child->pointer[child->size+1] = sibling->pointer[0];

                        adfs_bnode_key_rep(x, i, sibling, 0);


                        uint8_t j;
                        for(j = 1; j < sibling->size; j++) adfs_bnode_key_rep(sibling, j-1, sibling, j);

                        if(sibling->is_a_leaf == 0)
                        {
                                for(j = 1; i < sibling->size; j++) sibling->pointer[j-1] = sibling->pointer[j];
                        }

                        child->size++;
                        sibling->size--;

                        if(adfs_bnode_write(fs, x->pointer[i], child) == ADFS_FAILURE || adfs_bnode_write(fs, x->pointer[i+1], sibling) == ADFS_FAILURE || adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE)
                        {
                                free(child);
                                free(sibling);
                                return ADFS_FAILURE;
                        }
                        free(child);
                }
                free(sibling);
        }
        else
        {
                if(i != x->size)
                {
                        if(adfs_bnode_merge(fs, x_id, x, i) == ADFS_FAILURE) return ADFS_FAILURE;
                }
                else if(adfs_bnode_merge(fs, x_id, x, i-1) == ADFS_FAILURE) return ADFS_FAILURE;
        }

        return ADFS_SUCCESS;
}

uint8_t adfs_bnode_insert(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, const char* filename, uint8_t entry_type)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size*8-1) || x == NULL || filename == NULL || entry_type > 1) return ADFS_FAILURE;
        #endif

        int i = x->size-1;

        if(x->is_a_leaf)
        {
                while(i >= 0)
                {
                        if(x->name[i][0] == '\0') i--;
                        else if(strcmp(filename, x->name[i]) < 0)
                        {
                                strcpy(x->name[i+1], x->name[i]);
                                x->cluster[i+1] = x->cluster[i];
                                ADFS_BNODE_WRITE_ENTRY_TYPE(x->entry_type, i+1, ADFS_BNODE_READ_ENTRY_TYPE(x->entry_type, i));
                                i--;
                        }
                        else break;
                }

                strcpy(x->name[i+1], filename);
                ADFS_BNODE_WRITE_ENTRY_TYPE(x->entry_type, i+1, entry_type);

                if(entry_type == ADFS_BNODE_ENTRY_TYPE_DIR)
                {
                        adfs_bnode* node = NULL;
                        if((x->cluster[i+1] = adfs_bnode_prepare(fs, &node)) == 0) return ADFS_FAILURE;
                        if(adfs_bnode_write(fs, x->cluster[i+1], node) == ADFS_FAILURE) return ADFS_FAILURE;
                        free(node);
                }
                else if(entry_type == ADFS_BNODE_ENTRY_TYPE_FILE)
                {
                        adfs_inode* node = NULL;
                        if((x->cluster[i+1] = adfs_inode_prepare(fs, &node)) == 0) return ADFS_FAILURE;
                        if(adfs_inode_write(fs, x->cluster[i+1], node) == ADFS_FAILURE) return ADFS_FAILURE;
                        free(node);
                }
                else x->cluster[i+1] = 0; ///should NEVER HAPPEN (the function does not end here, though!)

                x->size++;

                if(adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE) return ADFS_FAILURE;
        }

        else
        {
                while(i >= 0)
                {
                        if(x->name[i][0] == '\0' || strcmp(filename, x->name[i]) < 0) i--;
                        else break;
                }

                uint32_t y_id = x->pointer[i+1];
                adfs_bnode* y = adfs_bnode_read(fs, y_id); //[MALLOC]

                if(y == NULL) return ADFS_FAILURE;

                if(y->size == (2*ADFS_BNODE_ORDER-1))
                {
                        if(adfs_bnode_split(fs, x_id, x, y_id, y, i+1) == ADFS_FAILURE) return ADFS_FAILURE;

                        if(strcmp(filename, x->name[i+1]) > 0) i++;
                }

                free(y); //[FREE]

                y_id = x->pointer[i+1];

                y = adfs_bnode_read(fs, y_id); //[MALLOC]
                if(y == NULL) return ADFS_FAILURE;

                if(adfs_bnode_insert(fs, y_id, y, filename, entry_type) == ADFS_FAILURE) return ADFS_FAILURE;
                free(y);//[FREE]
        }

        return ADFS_SUCCESS;
}

void adfs_bnode_key_rep(adfs_bnode* a, size_t a_i, adfs_bnode* b, size_t b_i)
{
        ///adfs_bnode_key_replace
        ///one of the very few functions without error checking (sort of an inline function)
        strcpy(a->name[a_i], b->name[b_i]);
        a->cluster[a_i] = b->cluster[b_i];
        ADFS_BNODE_WRITE_ENTRY_TYPE(a->entry_type, a_i, ADFS_BNODE_READ_ENTRY_TYPE(b->entry_type, b_i));
}

uint8_t adfs_bnode_merge(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, uint8_t i)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size*8-1) || x == NULL || i > (2*ADFS_BNODE_ORDER-1)) return ADFS_FAILURE;
        #endif

        uint32_t child_id = x->pointer[i];
        uint32_t sibling_id = x->pointer[i+1];

        if(child_id == 0 || sibling_id == 0) return ADFS_FAILURE;

        adfs_bnode* child = adfs_bnode_read(fs, child_id);
        if(child == NULL) return ADFS_FAILURE;

        adfs_bnode* sibling = adfs_bnode_read(fs, sibling_id);
        if(sibling == NULL) return ADFS_FAILURE;

        adfs_bnode_key_rep(child, ADFS_BNODE_ORDER-1, x, i);

    uint8_t j;
    for(j = 0; j < sibling->size; j++) adfs_bnode_key_rep(child, j+ADFS_BNODE_ORDER, sibling, j);

    if(child->is_a_leaf == 0)
    {
            for(j = 0; j < sibling->size; j++) child->pointer[i+ADFS_BNODE_ORDER] = child->pointer[i];
    }

    for(j = i+1; i < x->size; j++) adfs_bnode_key_rep(x, i-1, x, i);
    for(j = i+2; j <= x->size; j++) x->pointer[j-1] = x->pointer[j];

    child->size += sibling->size + 1;
    x->size--;

        uint8_t retval = ADFS_SUCCESS;
    if(adfs_bnode_write(fs, child_id, child) == ADFS_FAILURE || adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE || adfs_bnode_release(fs, sibling_id) == ADFS_FAILURE) retval = ADFS_FAILURE;

    free(sibling);
    free(child);

    return retval;
}

uint32_t adfs_bnode_prepare(adfs_fs* fs, adfs_bnode** node)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || node == NULL) return 0;
        #endif

        uint32_t id = adfs_bnode_request(fs);
        if(id == 0) return 0;

        *node = (adfs_bnode*) malloc(sizeof(adfs_bnode));
        memset(*node, 0, sizeof(adfs_bnode));
        (*node)->is_a_leaf = 1;

        return id; //the node is NOT saved
}

adfs_bnode*        adfs_bnode_read(adfs_fs* fs, uint32_t id)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || id > fs->bnodes_bitmap_size*8) return NULL;
        #endif

        adfs_bnode* node = (adfs_bnode*) malloc(sizeof(adfs_bnode));
        if(node == NULL) return NULL;

        off_t position = fs->superblock.bnodes_data + id*ADFS_BNODE_SIZE;

        if(lseek(fs->file, position, SEEK_SET) == position)
        {
                if(read(fs->file, node, sizeof(adfs_bnode)) == sizeof(adfs_bnode)) return node; //success
        }

        free(node); //failure
        return NULL;
}

uint8_t        adfs_bnode_release(adfs_fs* fs, uint32_t id)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || id > (fs->bnodes_bitmap_size*8-1)) return ADFS_FAILURE;
        #endif

        uint64_t position = id/8;
        size_t j = 7 - (id % 8); //the reversed order

        fs->bnodes_bitmap[position] &= (~(1 << j));

        off_t offset = fs->superblock.bnodes_bitmap + position;

        if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;
        if(write(fs->file, fs->bnodes_bitmap + position, 1) != 1) return ADFS_FAILURE;

        return ADFS_SUCCESS;
}

uint8_t adfs_bnode_remove(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, const char* filename)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size*8-1) || filename == NULL) return ADFS_FAILURE;
        #endif

        ///find the first key that is greater or equal to the filename
        size_t i = 0;
        while (i < x->size && strcmp(x->name[i], filename) < 0) i++;

        char* name = malloc(strlen(x->name[i])+1);
        if(name == NULL) return ADFS_FAILURE;
        strcpy(name, x->name[i]);

        ///check presence in this node
        if(i < x->size && strcmp(x->name[i], filename) == 0)
        {
                if(x->is_a_leaf == 1)
                {
                        int j = i+1;
                        for(; j < x->size; j++) adfs_bnode_key_rep(x, j-1, x, j);

                        x->size--;

                        if(adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE)
                        {
                                free(name);
                                return ADFS_FAILURE;
                        }

                }
                else if(adfs_bnode_rmnleaf(fs, x_id, x, i) == ADFS_FAILURE)
                {
                        free(name);
                        return ADFS_FAILURE;
                }
        }
        else
        {
                if(x->is_a_leaf == 1) return ADFS_SUCCESS; //no such filename here (but it is a success)

                uint8_t flag = ((i == x->size) ? 1 : 0);

                adfs_bnode* y = adfs_bnode_read(fs, x->pointer[i]);
                if(y == NULL)
                {
                        free(name);
                        return ADFS_FAILURE;
                }

                if(y->size < ADFS_BNODE_ORDER)
                {
                        if(adfs_bnode_fill(fs, x_id, x, i) == ADFS_FAILURE) return ADFS_FAILURE;
                }

                free(y);

                if(flag && i > x->size)
                {
                        y = adfs_bnode_read(fs, x->pointer[i-1]);
                        if(y == NULL || adfs_bnode_remove(fs, x->pointer[i-1], y, name) == ADFS_FAILURE)
                        {
                                free(name);
                                return ADFS_FAILURE;
                        }
                }
                else
                {
                        y = adfs_bnode_read(fs, x->pointer[i]);
                        if(y == NULL || adfs_bnode_remove(fs, x->pointer[i], y, name) == ADFS_FAILURE)
                        {
                                free(name);
                                return ADFS_FAILURE;
                        }
                }

                free(y);
        }

        free(name);

        return ADFS_SUCCESS;
}

uint32_t adfs_bnode_request(adfs_fs* fs)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1) return 0;
        #endif

        ///a non-static request
        uint64_t position = 0;
        uint32_t p = 0; //free page area

        for(;position < fs->bnodes_bitmap_size; position++)
        {
                int j;
                for(j = 7; j >= 0; j--)
                {
                        if((fs->bnodes_bitmap[position] & (1 << j)) != 0) p++;
                        else
                        {
                                fs->bnodes_bitmap[position] |= (1 << j); //we set the appropriate bit

                                off_t offset = fs->superblock.bnodes_bitmap + position;

                                if(lseek(fs->file, offset, SEEK_SET) != offset) return 0;
                                if(write(fs->file, fs->bnodes_bitmap + position, 1) != 1) return 0;

                                return p;
                        }
                }
        }

        return 0; //0 means there is no space left
}

uint8_t adfs_bnode_rmnleaf(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, uint8_t i)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size*8-1) || x == NULL || i > (2*ADFS_BNODE_ORDER-1)) return ADFS_FAILURE;
        #endif

        char* name = malloc(strlen(x->name[i])+1);
        if(name == NULL) return ADFS_FAILURE;
        strcpy(name, x->name[i]);

        adfs_bnode* y_prev = adfs_bnode_read(fs, x->pointer[i]);
        if(y_prev == NULL) return ADFS_FAILURE;

        adfs_bnode* y_next = adfs_bnode_read(fs, x->pointer[i+1]);
        if(y_next == NULL) return ADFS_FAILURE;

        uint8_t retval = ADFS_SUCCESS;

        if(y_prev->size >= ADFS_BNODE_ORDER)
        {
                ///get the previous one
                adfs_bnode* z = y_prev;
                while(z->is_a_leaf == 0)
                {
                        adfs_bnode* temp = z;
                        z = adfs_bnode_read(fs, z->pointer[z->size]);

                        if(z == NULL)
                        {
                                free(temp);
                                free(y_next);
                                free(y_prev);
                                return ADFS_FAILURE;
                        }
                        free(temp);
                }

                adfs_bnode_key_rep(x, i, z, z->size-1);

                if(adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE ||        adfs_bnode_remove(fs, x->pointer[i], y_prev, z->name[z->size-1]) == ADFS_FAILURE)
                {
                        retval = ADFS_FAILURE;
                }

                free(z);
        }
        else if(y_next->size >= ADFS_BNODE_ORDER)
        {
                ///get the next one
                adfs_bnode* z = y_next;
                while(z->is_a_leaf == 0)
                {
                        adfs_bnode* temp = z;
                        z = adfs_bnode_read(fs, z->pointer[0]);

                        if(z == NULL)
                        {
                                free(temp);
                                free(y_next);
                                free(y_prev);
                                return ADFS_FAILURE;
                        }

                        free(temp);
                }

                adfs_bnode_key_rep(x, i+1, z, 0);

                if(adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE || adfs_bnode_remove(fs, x->pointer[i+1], y_next, z->name[0]) == ADFS_FAILURE)
                {
                        retval = ADFS_FAILURE;
                }

                free(z);
        }
        else if(adfs_bnode_merge(fs, x_id, x, i) == ADFS_FAILURE || adfs_bnode_remove(fs, x->pointer[i], y_prev, name) == ADFS_FAILURE)
        {
                retval = ADFS_FAILURE;
        }

        free(y_next);
        free(y_prev);
        free(name);

        return retval;
}

uint8_t adfs_bnode_rtchange(adfs_fs* fs, uint32_t r_id, uint32_t rr_id, uint32_t new_r_id)
{
        ///rtchange = change root
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || r_id > (fs->bnodes_bitmap_size*8-1) || rr_id > (fs->bnodes_bitmap_size*8-1) || new_r_id > (fs->bnodes_bitmap_size*8-1)) return ADFS_FAILURE;
        #endif

        if(rr_id == 0 && fs->superblock.root_dir_id == r_id) fs->superblock.root_dir_id = new_r_id; //the root directory
        else
        {
                adfs_bnode* rr = adfs_bnode_read(fs, rr_id);
                if(rr == NULL) return ADFS_FAILURE;

                int i = 0;
                for(; i < (2*ADFS_BNODE_ORDER-1); i++)
                {
                        if(rr->cluster[i] == r_id)
                        {
                                rr->cluster[i] = new_r_id;
                                break;
                        }
                }

                if(adfs_bnode_write(fs, rr_id, rr) == ADFS_FAILURE)
                {
                        free(rr);
                        return ADFS_FAILURE;
                }
                free(rr);
        }

        return ADFS_SUCCESS;
}

adfs_sres* adfs_bnode_search(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, const char* filename, uint32_t R_id, uint8_t *exists)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size*8-1) || x == NULL || filename == NULL || R_id > (fs->bnodes_bitmap_size*8-1)) return NULL;
        #endif

        ///an interesting case which happens rarely
        if(x->size == 0) return NULL;

        uint8_t i = 0;
        for(; i < x->size; i++)
        {
                if(strcmp(filename, x->name[i]) > 0) continue;
                else break;
        }

        if(strcmp(filename, x->name[i]) == 0)
        {
                if(exists == NULL)
                {
                        adfs_sres* result = malloc(sizeof(adfs_sres)); //[MALLOC]
                        if(result == NULL) return NULL; //that's problematic

                        result->x_id = x_id;
                        result->i = i;
                        result->entry_type = ADFS_BNODE_READ_ENTRY_TYPE(x->entry_type, i);
                        result->cluster = x->cluster[i];
                        result->root_id = R_id;

                        return result;
                }
                else
                {
                        *exists = 1;
                        return NULL;
                }
        }

        if(x->is_a_leaf)
        {
                if(exists != NULL) *exists = 0;
                 return NULL;
        }

        adfs_bnode* node = adfs_bnode_read(fs, x->pointer[i]);
        if(node == NULL) return NULL;

        if(exists == NULL)
        {
                adfs_sres* result = adfs_bnode_search(fs, x->pointer[i], node, filename, x_id, NULL);
                free(node);

                 return result;
        }
        else
        {
                adfs_bnode_search(fs, x->pointer[i], node, filename, x_id, exists); //the return value is of no importance
                free(node);

                return NULL;
        }
}

uint8_t adfs_bnode_split(adfs_fs* fs, uint32_t x_id, adfs_bnode* x, uint32_t y_id, adfs_bnode* y, uint32_t i)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || x_id > (fs->bnodes_bitmap_size*8-1) || x == NULL || y_id > (fs->bnodes_bitmap_size*8-1) || y == NULL || i > (2*ADFS_BNODE_ORDER-1)) return ADFS_FAILURE;
        #endif

        adfs_bnode *z = NULL;
        uint32_t z_id = adfs_bnode_prepare(fs, &z); //[MALLOC]

        if(z_id == 0 || z == NULL) return ADFS_FAILURE;

        z->is_a_leaf = y->is_a_leaf;
        z->size = ADFS_BNODE_ORDER-1;

        int j;
        for(j = 0; j < (ADFS_BNODE_ORDER-1); j++) adfs_bnode_key_rep(z, j, y, j+ADFS_BNODE_ORDER);

        if(y->is_a_leaf == 0)
        {
                for(j = 0; j < ADFS_BNODE_ORDER; j++) z->pointer[j] = y->pointer[j+ADFS_BNODE_ORDER];
        }

        y->size = ADFS_BNODE_ORDER-1;

        for(j = (x->size); j >= (int)(i+1); j--) x->pointer[j+1] = x->pointer[j];

        x->pointer[i+1] = z_id;

        for(j = (x->size-1); j >= (int)i; j--) adfs_bnode_key_rep(x, j+1, x, j);

        adfs_bnode_key_rep(x, i, y, ADFS_BNODE_ORDER-1);

        x->size++;

        //x is a father, y is a son of x, //z is a son of z
        if(adfs_bnode_write(fs, x_id, x) == ADFS_FAILURE || adfs_bnode_write(fs, y_id, y) == ADFS_FAILURE || adfs_bnode_write(fs, z_id, z) == ADFS_FAILURE) return ADFS_FAILURE;

        free(z);

        return ADFS_SUCCESS;
}

uint8_t adfs_bnode_write(adfs_fs* fs, uint32_t id, adfs_bnode* node)
{
        #ifdef ADFS_DEBUG
        if(fs == NULL || fs->file == -1 || id > fs->bnodes_bitmap_size*8 || node == NULL) return ADFS_FAILURE;
        #endif

        off_t offset = fs->superblock.bnodes_data + id*ADFS_BNODE_SIZE;
        if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;
        if(write(fs->file, node, sizeof(adfs_bnode)) != sizeof(adfs_bnode)) return ADFS_FAILURE;
        return ADFS_SUCCESS;
}
