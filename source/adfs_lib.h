/* "adfs_lib.h" written by GrzPab (grzpab.eu5.org) as a part of the "Designing a
*  File System" course. This piece of code, written in the C language, is
*  distributed with absolutely no warranty. Should any damage or loss happen to
*  any software or hardware products by using the knowledge included in this
*  code or the code itself, the author will have no liability towards any
*  person, entitle, company or government which has used the code. All rights
*  reserved. You may redistribute this software freely as long as you credit
*  GrzPab for his work and provide a link to this website (grzpab.eu5.org).
*/

#ifndef ADFS_LIB_0_3_H
#define ADFS_LIB_0_3_H

#include "adfs.h"
#include <fcntl.h> /* file control */
#include <unistd.h> /* system call wrapper functions */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#define ADFS_BNODE_SIZE			2048
#define ADFS_INODE_SIZE			128
#define ADFS_CLUSTER_SIZE		2048
#define ADFS_MAX_FILESIZE		0x03FFFFFF
#define ADFS_FILENAME_CHARACTERS	128
#define ADFS_DEBUG			/* for debugging purposes only */

/* ADFS_INODE */
#define ADFS_INODE_MAGIC_NUMBER		0xFF00AA11

struct adfs_inode { /* index node (used exclusively for files) */
	uint32_t magic_number;
	/* size + additional bit flags (there is still a space for them) */
	uint32_t size;
	uint32_t entry1[2]; /* first-level entry */

	struct adfs_inode_entry /* second-level entry */
	{
		uint32_t start; /* where does an entry begin? (cluster ID) */
		uint32_t length; /* where does an entry end (which cluster)? */
	} entry2[14];
	/* a file can be fragmented only into 15 parts */

	/* in the current version:
	*  entry2[n].length = 2^(n+2); //max 2^15\
	*/

}; /* the size of this structure should be 128 bytes */
typedef struct adfs_inode adfs_inode;

/* ADFS_BNODE */
#define ADFS_BNODE_ORDER		8
#define ADFS_BNODE_ENTRY_TYPE_FILE      0
#define ADFS_BNODE_ENTRY_TYPE_DIR       1

#define ADFS_BNODE_READ_ENTRY_TYPE(x, i)\
	(((x & (1 << (15 - (i)))) == 0) ?\
	ADFS_BNODE_ENTRY_TYPE_FILE : ADFS_BNODE_ENTRY_TYPE_DIR)

#define ADFS_BNODE_WRITE_ENTRY_TYPE(x, i, v)\
	(x = ((x & ~(1 << (15 - (i)))) | ((v) << (15 - (i)))))

struct adfs_bnode {
	char		name[2*ADFS_BNODE_ORDER-1][ADFS_FILENAME_CHARACTERS];
	/* for each name, (they) point to inode or bnode (file or directory) */
	uint32_t	cluster[2*ADFS_BNODE_ORDER-1];
	/* (they) point to consecutive bnodes */
	uint32_t	pointer[2*ADFS_BNODE_ORDER];
	/* bitmap indicating whether an entry (name) is file or directory */
	uint16_t	entry_type;
	uint8_t		is_a_leaf;
	uint8_t		size;
}; /* the size of this structure should be 2048 bytes */
typedef struct adfs_bnode adfs_bnode;

/* ADFS_FS */
#define ADFS_SUPERBLOCK_MAGIC_NUMBER_1  "ADFS\x00\x00\x00\x03"
#define ADFS_SUPERBLOCK_MAGIC_NUMBER_2  (~(0l))

struct adfs_list {
	void	*element;
	struct	adfs_list *next;
};
typedef struct adfs_list adfs_list;

struct adfs_fs {
	int file; /* a file system's file */

	struct adfs_superblock { /* file system superblock */
		/* "ADFS" 0x00 0x00 0x00 0x03 */
		char		magic_number_1[8];
		uint64_t	volume_size; /* fixed, in bytes */

		/* fixed, where is the B-trees' bitmap? (in bytes) */
		uint64_t	bnodes_bitmap;
		/* fixed, where do B-trees start? (in bytes) */
		uint64_t	bnodes_data;

		/* fixed, where is the inodes' bitmap? (in bytes) */
		uint64_t	inodes_bitmap;
		/* fixed, where do the inodes start? (in bytes) */
		uint64_t	inodes_data;

		/* fixed, where is the clusters's bitmap? (in bytes) */
		uint64_t	clusters_bitmap;
		/* fixed, where do the clusters start? (in bytes) */
		uint64_t	clusters_data;
		/* fixed, how many clusters (blocks) are available? */
		uint64_t	blocks_available;
		/* how many clusters (blocks) are already in use? */
		uint64_t	blocks_used;

		uint64_t	number_of_files; /* how many files are there? */
		/* how many directories are there? */
		uint64_t	number_of_dirs;
		/* bnode_id of the root directory */
		uint64_t	root_dir_id;
		/* the name of the file system */
		char		name[8*3];
	} superblock; /* the size of this structure should be 128 bytes */

	/* opened  directories and files */
	adfs_list       *opened_dirs_head;
	adfs_list       *opened_dirs_tail;
	adfs_list       *opened_files_head;
	adfs_list       *opened_files_tail;

	/* bitmaps: */
	unsigned char	*bnodes_bitmap;
	unsigned char	*inodes_bitmap;
	unsigned char	*clusters_bitmap;

	uint64_t	bnodes_bitmap_size;
	uint64_t	inodes_bitmap_size;
	uint64_t	clusters_bitmap_size;
};

struct adfs_file { /* directory is also a file */
	adfs_fs		*fs;

	uint32_t	root_id; /* the root of the bnode_id cluster */
	uint32_t	bnode_id; /* stotes the link to the inode_id */
	uint32_t	inode_id;
	adfs_inode      *inode;

	uint32_t	offset; /* current offset */
};

struct adfs_dir {
	adfs_fs	 *fs;

	uint32_t	root_id; /* the root of the parent_id b-node */
	uint32_t	parent_id; /* parent b-node */
	/* the b-node which is the directory's entry */
	uint32_t	bnode_id;
	uint32_t	number_of_files; /* inside (immutable) */

	struct adfs_dir_file {
		char			*filepath;
		struct  adfs_dir_file	*next;
	} *files, *current;
};

typedef struct adfs_dir_file adfs_entry;

typedef struct {
	uint32_t	x_id;
	uint32_t	root_id; /* parent of a 'x' */
	uint8_t		i;
	uint32_t	cluster;
	uint8_t		entry_type; /* filetype */
} adfs_sres; /* adfs_search_result(s) */

/* adfs_bnode private functions: */
uint8_t		adfs_bnode_add(adfs_fs *, uint32_t, adfs_bnode *, const char *,
			       uint32_t, uint8_t);
uint32_t	adfs_bnode_create(adfs_fs *);
uint8_t		adfs_bnode_delete(adfs_fs*, uint32_t, adfs_bnode *,
				  const char*, uint32_t);
uint8_t		adfs_bnode_fill(adfs_fs *, uint32_t, adfs_bnode *, uint8_t);
uint8_t		adfs_bnode_insert(adfs_fs *, uint32_t, adfs_bnode *,
				  const char *, uint8_t);
void		adfs_bnode_key_rep(adfs_bnode *, size_t, adfs_bnode *, size_t);
uint8_t		adfs_bnode_merge(adfs_fs *, uint32_t, adfs_bnode *, uint8_t);
uint32_t	adfs_bnode_prepare(adfs_fs *, adfs_bnode **);
adfs_bnode	*adfs_bnode_read(adfs_fs *, uint32_t);
uint8_t		adfs_bnode_release(adfs_fs *, uint32_t);
uint8_t		adfs_bnode_remove(adfs_fs *, uint32_t, adfs_bnode *,
				  const char *);
uint32_t	adfs_bnode_request(adfs_fs *);
uint8_t		adfs_bnode_rmnleaf(adfs_fs *, uint32_t, adfs_bnode *, uint8_t);
uint8_t		adfs_bnode_rtchange(adfs_fs *, uint32_t, uint32_t, uint32_t);
adfs_sres	*adfs_bnode_search(adfs_fs *, uint32_t, adfs_bnode *,
				   const char*, uint32_t, uint8_t *);
uint8_t		adfs_bnode_split(adfs_fs*, uint32_t, adfs_bnode*,
					 uint32_t, adfs_bnode*, uint32_t);
uint8_t		adfs_bnode_write(adfs_fs*, uint32_t, adfs_bnode*);

/* adfs_cluster private functions: */
uint8_t		adfs_cluster_fill(adfs_fs *, uint32_t);
uint8_t		adfs_cluster_read(adfs_fs *, uint32_t, uint16_t, void *,
				  uint16_t);
uint8_t		adfs_cluster_release(adfs_fs *, uint32_t, uint16_t);
uint32_t	adfs_cluster_request(adfs_fs *, uint16_t);
uint8_t		adfs_cluster_write(adfs_fs *, uint32_t, uint16_t, const void *,
				   uint16_t);

/* adfs_dir private functions: */
uint8_t		adfs_dir_close(adfs_dir *);
uint8_t		adfs_dir_create(adfs_fs *, const char *);
uint8_t		adfs_dir_delete(adfs_fs *, const char *);
void		*adfs_dir_execute(adfs_fs *, const char *, uint8_t);
uint32_t	adfs_dir_filenumber(adfs_dir *);
uint8_t		adfs_dir_is_root(const char *);
adfs_dir	*adfs_dir_open(adfs_fs *, const char *);
char		*adfs_dir_read(adfs_dir *);
uint8_t		adfs_dir_rename(adfs_fs *, const char *, const char *);
uint32_t	adfs_dir_retr_size(adfs_fs*, uint32_t);
adfs_entry	*adfs_dir_retrieve(adfs_fs *, uint32_t, adfs_entry *,
				   uint32_t*);
uint8_t		adfs_dir_rewind(adfs_dir *);
uint8_t		adfs_dir_rm_rec(adfs_fs *, uint32_t);
uint32_t	adfs_dir_size(adfs_fs *, const char *);

/* adfs_file private functions: */
uint8_t		adfs_file_close(adfs_file *);
uint8_t		adfs_file_create(adfs_fs *, const char *);
uint8_t		adfs_file_delete(adfs_fs *, const char *);
uint8_t		adfs_file_exists(adfs_fs *, const char *);
adfs_file	*adfs_file_open(adfs_fs *, const char *);
uint32_t	adfs_file_read(adfs_file *, void *, size_t);
uint8_t		adfs_file_remove(adfs_fs *, uint32_t, adfs_inode *);
uint8_t		adfs_file_rename(adfs_fs *, const char *, const char *);
uint32_t	adfs_file_seek(adfs_file *, uint32_t);
uint32_t	adfs_file_write(adfs_file *, const void *, size_t);

/* adfs_fs private functions: */
uint8_t		adfs_fs_close(adfs_fs *);
adfs_fs		*adfs_fs_create(char *, char *, uint64_t);
adfs_fs		*adfs_fs_open(char *);
uint8_t		adfs_list_close(adfs_list **, adfs_list **);
uint8_t		adfs_list_insert(adfs_list **, adfs_list **, void *);
uint8_t		adfs_list_remove(adfs_list **, adfs_list **, void *);
uint64_t	adfs_number_of_dirs(adfs_fs *);
uint64_t	adfs_number_of_files(adfs_fs *);
char		*adfs_path_last(const char *);
char		*adfs_path_next(char *, uint8_t *, size_t *);

/* ADFS_INODE PRIVATE FUNCTIONS: */
uint32_t	adfs_inode_accsize(adfs_inode *);
uint32_t	adfs_inode_cluster(adfs_inode *, uint32_t);
uint32_t	adfs_inode_create(adfs_fs *);
uint32_t	adfs_inode_prepare(adfs_fs *, adfs_inode **);
adfs_inode	*adfs_inode_read(adfs_fs *, uint32_t);
uint8_t		adfs_inode_release(adfs_fs *, uint32_t);
uint32_t	adfs_inode_request(adfs_fs *);
uint8_t		adfs_inode_resize(adfs_fs *, adfs_inode *, uint32_t, uint8_t);
uint8_t		adfs_inode_write(adfs_fs *, uint32_t, adfs_inode *);

#define ADFS_DIR_CREATE_FUNCTION	1
#define ADFS_DIR_OPEN_FUNCTION		2
#define ADFS_DIR_DELETE_FUNCTION	3
#define ADFS_DIR_SIZE_FUNCTION		4
#define ADFS_FILE_CREATE_FUNCTION	5
#define ADFS_FILE_OPEN_FUNCTION		6
#define ADFS_FILE_DELETE_FUNCTION	7
#define ADFS_FILE_EXISTS_FUNCTION	8

#endif
