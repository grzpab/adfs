/*
"adfs_file.c" written by GrzPab (grzpab.eu5.org) as a part of the "Designing a File System" course.
This piece of code, written in the C language, is distributed with absolutely no warranty. Should any damage or loss happen to any software or hardware products by using the knowledge included in this code or the code itself, the author will have no liability towards any person, entitle, company or government which has used the code.
All rights reserved. You may redistribute this software freely as long as you credit GrzPab for his work and provide a link to this website (grzpab.eu5.org).
*/

#include "adfs_lib.h"

//adfs_file private functions:

uint8_t adfs_file_close(adfs_file* file)
{
	if(file == NULL || file->fs == NULL || file->fs->file == -1) return ADFS_FAILURE;

	adfs_fs* fs = file->fs;

	///save the inode
	if(adfs_inode_write(fs, file->inode_id, file->inode) == ADFS_FAILURE) return ADFS_FAILURE;

	///remove the file's pointer from the list of opened files within the file system
	adfs_list_remove(&(fs->opened_files_head), &(fs->opened_files_tail), file);
	///no error checking there, because the list is of a lesser importance

	free(file->inode);
	free(file);

	return ADFS_SUCCESS;
}

uint8_t adfs_file_create(adfs_fs* fs, const char* filepath)
{
	if(fs == NULL || fs->file == -1 || filepath == NULL) return ADFS_FAILURE;

	uint8_t* success = (uint8_t*) adfs_dir_execute(fs, filepath, ADFS_FILE_CREATE_FUNCTION);
	if(success == NULL) return ADFS_FAILURE; //problems with arguments, probably?

	uint8_t redval = *success;
	free(success);

	if(redval == 1) fs->superblock.number_of_files++; //statistics

	return redval;
}

uint8_t adfs_file_delete(adfs_fs* fs, const char* filepath)
{
	if(fs == NULL || fs->file == -1 || filepath == NULL) return ADFS_FAILURE;

	adfs_file* file = adfs_dir_execute(fs, filepath, ADFS_FILE_DELETE_FUNCTION);
	if(file == NULL) return ADFS_FAILURE; //there was nothing to get rid of

	///inode_id
	if(adfs_file_remove(fs, file->inode_id, file->inode) == ADFS_FAILURE)
	{
		free(file->inode);
		free(file);
		return ADFS_FAILURE;
	}
	free(file->inode); //it is still a pointer

	///bnode_id
	adfs_bnode* bnode = adfs_bnode_read(fs, file->bnode_id);
	if(bnode == NULL)
	{
		free(file);
		return ADFS_FAILURE;
	}

	uint8_t retval = ADFS_SUCCESS;

	///obtain the filename
	char* filename = adfs_path_last(filepath);
	if(filename != NULL) ///delete the proper entry
	{
		if(adfs_bnode_delete(fs, file->bnode_id, bnode, filename, file->root_id) == ADFS_FAILURE) retval = ADFS_FAILURE;
	}

	///cleaning
	free(filename);
	free(bnode);
	free(file);

	return retval;
}

uint8_t	adfs_file_exists(adfs_fs* fs, const char* dirpath)
{
	uint8_t* success = (uint8_t*) adfs_dir_execute(fs, dirpath, ADFS_FILE_EXISTS_FUNCTION);
	if(success == NULL) return ADFS_FAILURE; //problems with arguments, probably?
	uint8_t redval = *success;
	free(success);

	return redval;
}

adfs_file* adfs_file_open (adfs_fs* fs, const char* filepath)
{
	return adfs_dir_execute(fs, filepath, ADFS_FILE_OPEN_FUNCTION);
}

uint32_t adfs_file_read(adfs_file* file, void* buffer, size_t bytes)
{
	if(file == NULL || file->fs == NULL || file->fs->file == -1 || buffer == NULL || bytes == 0) return 0; //0 bytes is written

	if(file->offset + bytes > file->inode->size) bytes = file->inode->size - file->offset;
	if(bytes == 0) return 0;

	uint32_t cluster = file->offset / ADFS_CLUSTER_SIZE;
	uint32_t cluster_offset = file->offset % ADFS_CLUSTER_SIZE;

	uint32_t total_clusters = ((bytes - cluster_offset) / ADFS_CLUSTER_SIZE) + (((bytes - cluster_offset) % ADFS_CLUSTER_SIZE == 0) ? 0 : 1);

	size_t bytes_written = 0;

	uint32_t i = 0;
	for(; i < total_clusters; i++, cluster++)
	{
		uint32_t cluster_id = adfs_inode_cluster(file->inode, cluster);

		if(i == 0)
		{
			size_t max_bytes = ADFS_CLUSTER_SIZE - cluster_offset;
			size_t bytes_to_write = (bytes > max_bytes) ? max_bytes : bytes;

			if(adfs_cluster_read(file->fs, cluster_id, cluster_offset, buffer, bytes_to_write) == ADFS_FAILURE) return 0;
			bytes_written += bytes_to_write;
		}
		else if(i < total_clusters-1)
		{
			if(adfs_cluster_read(file->fs, cluster_id, 0, buffer + bytes_written, ADFS_CLUSTER_SIZE) == ADFS_FAILURE) return 0;
			bytes_written += ADFS_CLUSTER_SIZE;
		}
		else
		{
			if(adfs_cluster_read(file->fs, cluster_id, 0, buffer + bytes_written, bytes - bytes_written) == ADFS_FAILURE) return 0;
			bytes_written += bytes - bytes_written;
		}
	}

	file->offset += bytes; //the offset has been changed

	return bytes;
}

uint8_t adfs_file_remove(adfs_fs* fs, uint32_t inode_id, adfs_inode* inode)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || inode_id > (fs->inodes_bitmap_size*8-1) || inode == NULL || inode->magic_number != ADFS_INODE_MAGIC_NUMBER) return ADFS_FAILURE;
	#endif

	///check whether is opened or not! (using file->inode_id)
	adfs_list* current = fs->opened_files_head;

	while(current != fs->opened_files_tail)
	{
		adfs_file* to_check = current->element;

		if(to_check->inode_id == inode_id) return ADFS_FAILURE; ///it would be harsh to delete an opened file
		current = current->next;
	}

	///removing file clusters with no error checking (the data will be lost nevertheless)
	if(inode->entry1[0] != 0) adfs_cluster_release(fs, inode->entry1[0], 1);
	if(inode->entry1[1] != 0) adfs_cluster_release(fs, inode->entry1[1], 1);

	uint8_t i = 0;
	for(; (i < 14) && (inode->entry2[i].start != 0); i++)
	{
		adfs_cluster_release(fs, inode->entry2[i].start, inode->entry2[i].length);
	}

	///remove the inode
	if(adfs_inode_release(fs, inode_id) == ADFS_FAILURE) return ADFS_FAILURE;

	///statistics
	fs->superblock.number_of_files--;

	return ADFS_SUCCESS;
}

uint8_t	adfs_file_rename (adfs_fs* fs, const char* filepath, const char* new_filepath)
{
	///renaming is about deleting the bnode's entry of a particular file and creating a new one while inode and clusters remain untouched
	if(fs == NULL || fs->file == -1 || filepath == NULL || new_filepath == NULL) return ADFS_FAILURE;

	///step 1: open the old file
	adfs_file* file = adfs_dir_execute(fs, filepath, ADFS_FILE_DELETE_FUNCTION); ///it does not delete the file [MALLOC]
	if(file == NULL) return ADFS_FAILURE;

	///step 2: open the bnode which stores the pointer to the old file's inode
	adfs_bnode* bnode = adfs_bnode_read(fs, file->bnode_id); ///[MALLOC]
	if(bnode == NULL)
	{
		free(file->inode); free(file);
		return ADFS_FAILURE;
	}

	///step 3: create a new file
	uint8_t* crt = (uint8_t*) adfs_dir_execute(fs, new_filepath, ADFS_FILE_CREATE_FUNCTION);
	if(crt == NULL || *crt == ADFS_FAILURE)
	{
		free(bnode); free(file->inode);	free(file);
		return ADFS_FAILURE; //problems with arguments, probably?
	}
	free(crt);

	///step 4: open the newly-created file
	adfs_file* new_file = adfs_dir_execute(fs, new_filepath, ADFS_FILE_OPEN_FUNCTION); ///[MALLOC]
	if(new_file == NULL)
	{
		free(bnode); free(file->inode);	free(file);
		return ADFS_FAILURE;
	}

	///step 5: open the new file's bnode and link the inode to it
	//new_file->bnode_id is the bnode (parent) id
	adfs_bnode* new_bnode = adfs_bnode_read(fs, new_file->bnode_id);
	char* new_filename = adfs_path_last(new_filepath);
	if(new_bnode == NULL || new_filename == NULL)
	{
		if(new_bnode != NULL) free(new_bnode);
		if(new_filename != NULL) free(new_filename);
		free(new_file->inode); free(new_file); free(bnode);	free(file);
		return ADFS_FAILURE;
	}

	size_t i = 0;
	uint8_t is_found = ADFS_FAILURE;
	for(i = 0; i < new_bnode->size; i++)
	{
		if(strcmp(new_bnode->name[i], new_filename) == 0)
		{
			is_found = ADFS_SUCCESS;
			break;
		}
	}

	if(is_found == ADFS_FAILURE)
	{
		free(new_bnode);
		free(new_filename);
		free(new_file->inode); free(new_file);
		free(bnode);
		free(file->inode); free(file);
		return ADFS_FAILURE; //should not happen!
	}

	adfs_inode_release(fs, new_bnode->cluster[i]); ///no error checking here
	new_bnode->cluster[i] = file->inode_id; ///replacement

	if(adfs_bnode_write(fs, new_file->bnode_id, new_bnode) == ADFS_FAILURE) //a serious problem in this very moment
	{
		free(new_bnode);
		free(new_filename);
		free(new_file->inode); free(new_file);
		free(bnode);
		free(file->inode); free(file);
		return ADFS_FAILURE;
	}

	free(new_bnode);
	free(new_filename);
	free(new_file->inode); free(new_file);
	free(file->inode);

	///step 6: delete the inode's entry in the proper bnode (+ obtain the filename)
	uint8_t retval = ADFS_SUCCESS;

	char* filename = adfs_path_last(filepath);
	if(filename != NULL)
	{
		if(adfs_bnode_delete(fs, file->bnode_id, bnode, filename, file->root_id) == ADFS_FAILURE) retval = ADFS_FAILURE;
	}

	free(filename);
	free(bnode);
	free(file);

	return retval;
}

uint32_t adfs_file_seek(adfs_file* file, uint32_t offset)
{
	if(file == NULL || file->fs == NULL || file->fs->file == -1 || offset >= ADFS_MAX_FILESIZE) return 0;

	if(file->offset >= file->inode->size)
	{
		file->offset = offset;
		return offset;
	}

	///the newest offset is greater than the size
	if(adfs_inode_resize(file->fs, file->inode, file->offset, 1) == 0) return file->offset; //current offset

	file->offset = offset;
	file->inode->size = offset;

	adfs_inode_write(file->fs, file->inode_id, file->inode); //save the inode
	///if the preceding function has failed, there is a problem (nothing to be done, though)

	return offset;
}

uint32_t adfs_file_write(adfs_file* file, const void* buffer, size_t bytes)
{
	if(file == NULL || file->fs == NULL || file->fs->file == -1 || buffer == NULL || bytes == 0) return 0;

	if(adfs_inode_resize(file->fs, file->inode, file->offset + bytes, 0) == ADFS_FAILURE) return 0;
	///we have enough space to write our data

	uint32_t cluster = file->offset / ADFS_CLUSTER_SIZE;
	uint32_t cluster_offset = file->offset % ADFS_CLUSTER_SIZE;

	uint32_t total_clusters = ((bytes - cluster_offset) / ADFS_CLUSTER_SIZE) + (((bytes - cluster_offset) % ADFS_CLUSTER_SIZE == 0) ? 0 : 1);

	size_t bytes_written = 0;

	uint32_t i = 0;
	for(; i < total_clusters; i++, cluster++)
	{
		uint32_t cluster_id = adfs_inode_cluster(file->inode, cluster); //it should be greater than 0
		if(cluster_id == 0) return 0;

		if(i == 0)
		{
			size_t max_bytes = ADFS_CLUSTER_SIZE - cluster_offset;
			size_t bytes_to_write = (bytes > max_bytes) ? max_bytes : bytes;

			if(adfs_cluster_write(file->fs, cluster_id, cluster_offset, buffer, bytes_to_write) == ADFS_FAILURE) return 0;
			bytes_written += bytes_to_write;
		}
		else if(i < total_clusters-1)
		{
			if(adfs_cluster_write(file->fs, cluster_id, 0, buffer + bytes_written, ADFS_CLUSTER_SIZE) == ADFS_FAILURE) return 0;
			bytes_written += ADFS_CLUSTER_SIZE;
		}
		else
		{
			if(adfs_cluster_write(file->fs, cluster_id, 0, buffer + bytes_written, bytes - bytes_written) == ADFS_FAILURE) return 0;
			bytes_written += bytes - bytes_written;
		}
	}

	///make changes to the on-disk structures
	file->offset += bytes; //the offset has been changed
	if(file->offset > file->inode->size) file->inode->size = file->offset;

	if(adfs_inode_write(file->fs, file->inode_id, file->inode) == ADFS_FAILURE) return 0;

	return bytes;
}
