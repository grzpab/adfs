/*
"adfs_inode.c" written by GrzPab (grzpab.eu5.org) as a part of the "Designing a File System" course.
This piece of code, written in the C language, is distributed with absolutely no warranty. Should any damage or loss happen to any software or hardware products by using the knowledge included in this code or the code itself, the author will have no liability towards any person, entitle, company or government which has used the code.
All rights reserved. You may redistribute this software freely as long as you credit GrzPab for his work and provide a link to this website (grzpab.eu5.org).
*/

#include "adfs_lib.h"

//adfs_inode private functions:

uint32_t adfs_inode_accsize(adfs_inode* inode)
{
	///adfs_inode_accessible_size
	#ifdef ADFS_DEBUG
	if(inode == NULL || inode->magic_number != ADFS_INODE_MAGIC_NUMBER) return 0; //no inode = no size
	#endif

	if(inode->entry1[0] == 0) return 0;
	if(inode->entry1[1] == 0) return ADFS_CLUSTER_SIZE;

	size_t i = 0;
	uint32_t size = ADFS_CLUSTER_SIZE;

	for(; i < 14; i++)
	{
		if(inode->entry2[i].start != 0)	size += inode->entry2[i].length*ADFS_CLUSTER_SIZE;
		else break;
	}

	return size;
}

uint32_t adfs_inode_cluster(adfs_inode* inode, uint32_t number)
{
	#ifdef ADFS_DEBUG
	if(inode == NULL || inode->magic_number != ADFS_INODE_MAGIC_NUMBER) return 0;
	#endif

	///fetches cluster id
	//the "number " variable starts from 0
	if(number < 2) return inode->entry1[number];

	//second-level entries
	size_t i = 0;
	uint32_t j = 1; //the current cluster, as the number was "1"

	for(; i < 14; i++)
	{
		if((inode->entry2[i].length + j) >= number)	return (inode->entry2[i].start + number - j - 1);
		else j += inode->entry2[i].length;
	}

	return 0; //failure
}

uint32_t adfs_inode_create(adfs_fs* fs)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1) return 0;
	#endif

	uint32_t id = adfs_inode_request(fs);
	if(id == 0) return 0;

	adfs_inode node;
	memset(&node, 0, sizeof(adfs_inode));
	node.magic_number = ADFS_INODE_MAGIC_NUMBER;
	if(adfs_inode_write(fs, id, &node) == ADFS_FAILURE) return 0; //has not been written

	return id;
}

uint32_t adfs_inode_prepare(adfs_fs* fs, adfs_inode** node)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || node == NULL) return 0;
	#endif

	uint32_t id = adfs_inode_request(fs);
	if(id == 0) return 0;

	*node = (adfs_inode*) malloc(sizeof(adfs_inode));
	memset(*node, 0, sizeof(adfs_inode));
	//set the variables inside:
	(*node)->magic_number = ADFS_INODE_MAGIC_NUMBER;

	return id; //NOT SAVED
}

adfs_inode* adfs_inode_read(adfs_fs* fs, uint32_t id)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || id > (fs->inodes_bitmap_size*8-1)) return NULL;
	#endif

	adfs_inode* node = (adfs_inode*) malloc(sizeof(adfs_inode));
	if(node == NULL) return NULL;

	off_t offset = fs->superblock.inodes_data + id*ADFS_INODE_SIZE;

	if(lseek(fs->file, offset, SEEK_SET) == offset)
	{
		if(read(fs->file, node, sizeof(adfs_inode)) == sizeof(adfs_inode)) return node;
	}

	free(node); //failure
	return NULL;
}

uint8_t adfs_inode_release (adfs_fs* fs, uint32_t id)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || id > (fs->inodes_bitmap_size*8-1)) return ADFS_FAILURE;
	#endif

	uint64_t position = id/8;
	size_t j = 7 - (id % 8); //the reversed order

	fs->inodes_bitmap[position] &= (~(1 << j));

	off_t offset = fs->superblock.inodes_bitmap + position;

	if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;
	if(write(fs->file, fs->inodes_bitmap + position, 1) != 1) return ADFS_FAILURE;

	return ADFS_SUCCESS;
}

uint32_t adfs_inode_request(adfs_fs* fs)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1) return 0;
	#endif

	uint64_t position = 0;
	uint32_t p = 0; //free page area

	for(;position < fs->inodes_bitmap_size; position++)
	{
		int j;
		for(j = 7; j >= 0; j--)
		{
			if((fs->inodes_bitmap[position] & (1 << j)) != 0) p++;
			else
			{
				fs->inodes_bitmap[position] |= (1 << j); //we set the appropriate bit

				off_t offset = fs->superblock.inodes_bitmap + position;

				if(lseek(fs->file, offset, SEEK_SET) != offset) return 0;
				if(write(fs->file, fs->inodes_bitmap + position, 1) != 1) return 0;

				return p;
			}
		}
	}

	return 0; //0 means there is no space left
}

uint8_t adfs_inode_resize(adfs_fs* fs, adfs_inode* inode, uint32_t new_size, uint8_t fill_with_zeros)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || inode == NULL || inode->magic_number != ADFS_INODE_MAGIC_NUMBER || new_size == 0) return ADFS_FAILURE;
	#endif

	uint32_t accessible_size = adfs_inode_accsize(inode);
	if(new_size <= accessible_size) return ADFS_SUCCESS; //nothing to do

	uint32_t clustered_new_size = (new_size / ADFS_CLUSTER_SIZE) + (((new_size % ADFS_CLUSTER_SIZE) == 0) ? 0 : 1);
	uint32_t clusters_needed = clustered_new_size - accessible_size / ADFS_CLUSTER_SIZE;

	size_t i = 0; //entry

	while(clusters_needed != 0)
	{
		if(i < 2) //0,1
		{//first-level entry
			if(inode->entry1[i] == 0)
			{
				if((inode->entry1[i] = adfs_cluster_request(fs, 1)) == 0) return ADFS_FAILURE; //no space left?
				if(fill_with_zeros) adfs_cluster_fill(fs, inode->entry1[i]); //failure here is of no importance

				clusters_needed--;
			}
		}
		else if(i < 16) //2...15
		{//second-level entry
			if(inode->entry2[i-2].start == 0)
			{
				uint16_t length = 1 << (i-1); //2^(i-1)
				if((inode->entry2[i-2].start = adfs_cluster_request(fs, length)) == 0) return ADFS_FAILURE; //no space left?
				inode->entry2[i-2].length = length;

				if(fill_with_zeros)
				{
					uint32_t filled_cluster = inode->entry2[i-2].start;

					uint16_t j = 0;
					for(; j < length; j++, filled_cluster++) adfs_cluster_fill(fs, filled_cluster); //failure here is of no importance
				}

				clusters_needed = (clusters_needed >= length) ? (clusters_needed - length) : 0;
			}
		}
		else return ADFS_FAILURE; //it's a serious error - should never happen

		i++; //next entry
	}

	///the inode is NOT saved
	return ADFS_SUCCESS; //something has been done
}

uint8_t adfs_inode_write(adfs_fs* fs, uint32_t id, adfs_inode* node)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || id > (fs->inodes_bitmap_size*8-1) || node == NULL) return ADFS_FAILURE;
	#endif

	off_t offset = fs->superblock.inodes_data + id*ADFS_INODE_SIZE;

	if(lseek(fs->file, offset, SEEK_SET) != offset) return ADFS_FAILURE;
	if(write(fs->file, node, sizeof(adfs_inode)) != sizeof(adfs_inode)) return ADFS_FAILURE;

	return ADFS_SUCCESS;
}
