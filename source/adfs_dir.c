/*
"adfs_dir.c" written by GrzPab (grzpab.eu5.org) as a part of the "Designing a File System" course.
This piece of code, written in the C language, is distributed with absolutely no warranty. Should any damage or loss happen to any software or hardware products by using the knowledge included in this code or the code itself, the author will have no liability towards any person, entitle, company or government which has used the code.
All rights reserved. You may redistribute this software freely as long as you credit GrzPab for his work and provide a link to this website (grzpab.eu5.org).
*/

#include "adfs_lib.h"

//adfs_dir private functions:

uint8_t adfs_dir_close(adfs_dir* dir)
{
	if(dir == NULL) return ADFS_FAILURE;

	if(dir->files != NULL)
	{
		struct adfs_dir_file* handle1 = dir->files;
		struct adfs_dir_file* handle2 = dir->files->next;

		while(1)
		{
			free(handle1->filepath); //have to free this
			free(handle1);

			if(handle2 == NULL) break;

			handle1 = handle2;
			handle2 = handle1->next;
		}
	}

	///remove the directory's pointer from the list of opened directories within the file system
	adfs_fs* fs = dir->fs; //alias
	if(fs != NULL) adfs_list_remove(&(fs->opened_dirs_head), &(fs->opened_dirs_tail), dir); //return value checking?

	free(dir);

	return ADFS_SUCCESS;
}

uint8_t adfs_dir_create(adfs_fs* fs, const char* dirpath)
{
	uint8_t* success = (uint8_t*) adfs_dir_execute(fs, dirpath, ADFS_DIR_CREATE_FUNCTION);
	if(success == NULL) return 0; //problems with arguments, probably?

	uint8_t  redval = *success;
	free(success);

	if(redval == 1)	fs->superblock.number_of_dirs++; //statistics

	return redval;
}

uint8_t	adfs_dir_delete(adfs_fs* fs, const char* dirpath)
{
	if(fs == NULL || fs->file == -1 || dirpath == NULL) return ADFS_FAILURE;

	adfs_dir* dir = adfs_dir_execute(fs, dirpath, ADFS_DIR_DELETE_FUNCTION);
	if(dir == NULL) return ADFS_FAILURE;

	if(dir->bnode_id == fs->superblock.root_dir_id)
	{
		///one cannot delete the root directory
		free(dir);
		return ADFS_FAILURE;
	}

	///find out whether this directory has already been opened (dir->bnode_id)
	adfs_list* current = fs->opened_dirs_head;

	while(current != fs->opened_dirs_tail)
	{
		adfs_dir* to_check = current->element;
		if(to_check->bnode_id == dir->bnode_id)
		{
			free(dir);
			return ADFS_FAILURE;
		}
		current = current->next;
	}

	///we need to remove the entire directory:
	///go through bnode_id:
	adfs_dir_rm_rec(fs, dir->bnode_id); //no error checking (the data will be lost nevertheless)

	///remove the actual directory (inside the root directory):
	///obtain the filename
	uint8_t retval = ADFS_SUCCESS;

	char* dirname = adfs_path_last(dirpath);
	if(dirname != NULL)
	{
		///delete the proper entry from the parent bnode
		adfs_bnode* parent = adfs_bnode_read(fs, dir->parent_id);
		if(parent != NULL)
		{
			if(adfs_bnode_delete(fs, dir->parent_id, parent, dirname, dir->root_id) == ADFS_FAILURE) retval = ADFS_FAILURE;
			free(parent);
		}
		else retval = ADFS_FAILURE;
	}

	free(dirname);
	adfs_dir_close(dir); //a smart move

	return retval;
}

void* adfs_dir_execute(adfs_fs* fs, const char* dirpath, uint8_t function)
{
	if(fs == NULL || dirpath == NULL || fs->file == -1) return NULL;

	///start from the root directory
	uint32_t root_id = fs->superblock.root_dir_id;
	uint32_t root_of_root_id = 0;

	adfs_bnode* root = adfs_bnode_read(fs, root_id); //[MALLOC]
	if(root == NULL) return NULL;

	///go through the subdirectories:
	char* new_path = malloc(strlen(dirpath)+1);
	if(new_path == NULL) return NULL;
	strcpy(new_path, dirpath);

	char* a = new_path;
	uint8_t end = 0;
	size_t increment = 0;

	///return variables
	uint8_t success = 0;
	uint32_t dirsize = 0;
	adfs_dir* dir = NULL;
	adfs_file* file = NULL;

	///the directory's path may be the path of the root directory (lone '/' signs)
	uint8_t root_dirpath = adfs_dir_is_root(dirpath);

	///search for it
	adfs_sres* result = NULL;

	while(end == 0)
	{
		if(root_dirpath)
		{
			end = 1;
			if(function <= ADFS_DIR_SIZE_FUNCTION || function == ADFS_FILE_EXISTS_FUNCTION) success = 1; //root directory cannot be open as a file!
		}
		else
		{
			a = adfs_path_next(a, &end, &increment);
			if(a == NULL) break;

			result = adfs_bnode_search(fs, root_id, root, a, root_of_root_id, (function == ADFS_FILE_EXISTS_FUNCTION && end == 1)?(&success):NULL);
		}

		if(end == 1)
		{
			if(function == ADFS_DIR_CREATE_FUNCTION || function == ADFS_FILE_CREATE_FUNCTION)
			{
				if(result == NULL && root_dirpath == 0)//it should NOT EXIST
				{
					adfs_bnode_add(fs, root_id, root, a, root_of_root_id, (function == ADFS_DIR_CREATE_FUNCTION) ? ADFS_BNODE_ENTRY_TYPE_DIR : ADFS_BNODE_ENTRY_TYPE_FILE); ///no error checking
					///a proper bnode has been added (if it has been a file, an inode has been created too)
					success = 1;
					break;
				}
			}
			else if(function == ADFS_DIR_OPEN_FUNCTION || function == ADFS_DIR_DELETE_FUNCTION)
			{
				if(result == NULL && root_dirpath == 0) break;//it has to EXIST

				//create a list
				dir = malloc(sizeof(adfs_dir));

				if(dir != NULL)
				{
					dir->fs = fs;
					dir->number_of_files = 0;

					if(root_dirpath == 1)
					{
						dir->root_id = 0; //does not exist
						dir->parent_id = 0; //does not exist
						dir->bnode_id = fs->superblock.root_dir_id;
					}
					else
					{
						dir->root_id = result->root_id;
						dir->parent_id = result->x_id;
						dir->bnode_id = result->cluster;
					}

					adfs_entry adf; //no need of memsetting
					adf.next = NULL; ///very important!

					///retrieve all elements (no error checking)
					adfs_dir_retrieve(fs, (root_dirpath == 0) ? result->cluster : fs->superblock.root_dir_id, &adf, &(dir->number_of_files));
					dir->files = adf.next;
					dir->current = dir->files;

					if(function == ADFS_DIR_OPEN_FUNCTION)
					{
						///add this directory to the list of opened directories within the file system
						adfs_list_insert(&(fs->opened_dirs_head), &(fs->opened_dirs_tail), dir);
						///no error checking
					}

				}
			}
			else if(function == ADFS_FILE_OPEN_FUNCTION || function == ADFS_FILE_DELETE_FUNCTION)
			{
				if(result == NULL) break; //a file has to exist

				file = malloc(sizeof(adfs_file));
				if(file != NULL)
				{
					file->fs = fs;
					file->root_id = result->root_id;
					file->bnode_id = result->x_id; //root_id is a parent of x_id
					file->inode_id = result->cluster;
					file->inode = adfs_inode_read(fs, file->inode_id);
					file->offset = 0; //we start from the 0-th byte

					if(function == ADFS_FILE_OPEN_FUNCTION)
					{
						///add this file to the list of opened files within the file system
						adfs_list_insert(&(fs->opened_files_head), &(fs->opened_files_tail), file);
						///no error checking
					}
				}
			}
			else if(function == ADFS_DIR_SIZE_FUNCTION)
			{
				if(result == NULL && root_dirpath == 0) break;//it has to EXIST

				dirsize = adfs_dir_retr_size(fs, (root_dirpath == 0) ? result->cluster : fs->superblock.root_dir_id); ///no error checking
			}

			if(result != NULL) free(result);
		}
		else
		{
			if(result == NULL) break; //it should EXIST

			if(result->entry_type != ADFS_BNODE_ENTRY_TYPE_DIR)
			{
				free(result);
				break;
			}

			root_of_root_id = result->x_id; //it's not result->root_id
			root_id = result->cluster;

			free(result);
			free(root);

			root = adfs_bnode_read(fs, root_id);
			if(root == NULL) return NULL;
		}

		a += increment;
	}
	///end of going through subdirectories

	free(root); //[FREE]
	free(new_path);

	///return an appropriate pointer
	switch(function) //no 'break'(s) needed
	{
		case ADFS_DIR_CREATE_FUNCTION:
		case ADFS_FILE_CREATE_FUNCTION:
		case ADFS_FILE_EXISTS_FUNCTION:
		{
			uint8_t* retval = malloc(sizeof(uint8_t));
			if(retval == NULL) return NULL;
			*retval = success;
			return (void*)retval;
		}
		case ADFS_DIR_SIZE_FUNCTION:
		{
			uint32_t* retval = malloc(sizeof(uint32_t));
			if(retval == NULL) return NULL;
			*retval = dirsize;
			return (void*)retval;
		}
		case ADFS_DIR_OPEN_FUNCTION:
		case ADFS_DIR_DELETE_FUNCTION:
			return dir;
		case ADFS_FILE_OPEN_FUNCTION:
		case ADFS_FILE_DELETE_FUNCTION:
			return file;
		default: return NULL;
	}
}

uint32_t adfs_dir_filenumber(adfs_dir* dir)
{
	if(dir == NULL) return 0; //no directory, no files inside

	return dir->number_of_files;
}

uint8_t adfs_dir_is_root(const char* dirpath)
{
	#ifdef ADFS_DEBUG
	if(dirpath == NULL) return ADFS_FAILURE;
	#endif

	size_t i = 0;
	while(i < strlen(dirpath))
	{
		if(dirpath[i++] != '/') return ADFS_FAILURE;
	}

	return ADFS_SUCCESS;
}

adfs_dir* adfs_dir_open(adfs_fs* fs, const char* dirpath)
{
	return adfs_dir_execute(fs, dirpath, ADFS_DIR_OPEN_FUNCTION);
}

char* adfs_dir_read(adfs_dir* dir)
{
	if(dir == NULL || dir->current == NULL) return NULL;

	char* path = malloc(strlen(dir->current->filepath)+1);
	if(path == NULL) return NULL;

	strcpy(path, dir->current->filepath);
	dir->current = dir->current->next;

	return path;
}

uint8_t adfs_dir_rename(adfs_fs* fs, const char* dirpath, const char* new_dirpath)
{
	if(fs == NULL || fs->file == -1 || dirpath == NULL || new_dirpath == NULL) return ADFS_FAILURE;

	///step 1: open the old directory and check whether it is a root directory or not
	adfs_dir* dir = adfs_dir_execute(fs, dirpath, ADFS_DIR_DELETE_FUNCTION); ///[MALLOC]
	if(dir == NULL) return ADFS_FAILURE;

	if(dir->bnode_id == fs->superblock.root_dir_id)
	{
		adfs_dir_close(dir);
		return ADFS_FAILURE;
	}

	///step 2: find out whether this directory has already been opened (dir->bnode_id)
	adfs_list* current = fs->opened_dirs_head;

	while(current != fs->opened_dirs_tail)
	{
		adfs_dir* to_check = current->element;
		if(to_check->bnode_id == dir->bnode_id)
		{
			adfs_dir_close(dir);
			return ADFS_FAILURE;
		}
		current = current->next;
	}

	///step 3: create a new directory
	uint8_t* crt = (uint8_t*) adfs_dir_execute(fs, new_dirpath, ADFS_DIR_CREATE_FUNCTION);
	if(crt == NULL || *crt == ADFS_FAILURE)
	{
		adfs_dir_close(dir);
		return ADFS_FAILURE;
	}
	free(crt);

	///step 4: open the newly-created directory:
	adfs_dir* new_dir = adfs_dir_execute(fs, new_dirpath, ADFS_DIR_OPEN_FUNCTION); ///[MALLOC]
	if(new_dir == NULL)
	{
		adfs_dir_close(dir);
		return ADFS_FAILURE;
	}

	///step 5: open the new directory's parent bnode and link the old one's bnode to it
	adfs_bnode* new_bnode = adfs_bnode_read(fs, new_dir->parent_id); ///[MALLOC]
	char* new_dirname = adfs_path_last(new_dirpath); ///[MALLOC]

	if(new_bnode == NULL || new_dirname == NULL)
	{
		if(new_bnode != NULL) free(new_bnode);
		if(new_dirname != NULL) free(new_dirname);
		adfs_dir_close(new_dir);
		adfs_dir_close(dir);
		return ADFS_FAILURE;
	}

	//find out where the link is stored
	size_t i = 0;
	uint8_t is_found = ADFS_FAILURE;
	for(i = 0; i < new_bnode->size; i++)
	{
		if(strcmp(new_bnode->name[i], new_dirname) == 0)
		{
			is_found = ADFS_SUCCESS;
			break;
		}
	}

	if(is_found == ADFS_FAILURE) //should never happen
	{
		free(new_dirname);
		free(new_bnode);
		adfs_dir_close(new_dir);
		adfs_dir_close(dir);
		return ADFS_FAILURE;
	}

	adfs_bnode_release(fs, new_bnode->cluster[i]); ///no error checking
	new_bnode->cluster[i] = dir->bnode_id;

	if(adfs_bnode_write(fs, new_dir->parent_id, new_bnode) == ADFS_FAILURE) //a serious problem in this very moment
	{
		free(new_dirname);
		free(new_bnode);
		adfs_dir_close(new_dir);
		adfs_dir_close(dir);
		return ADFS_FAILURE;
	}

	free(new_dirname);
	free(new_bnode);
	adfs_dir_close(new_dir);

	///step 6: delete the bnode's entry in the proper bnode:
	uint8_t retval = ADFS_SUCCESS;

	char* dirname = adfs_path_last(dirpath);
	if(dirname != NULL)
	{
		adfs_bnode* bnode = adfs_bnode_read(fs, dir->parent_id);
		if(bnode == NULL || adfs_bnode_delete(fs, dir->parent_id, bnode, dirname, dir->root_id) == ADFS_FAILURE) retval = ADFS_FAILURE;

		if(bnode != NULL) free(bnode);
	}

	free(dirname);
	adfs_dir_close(dir);

	return retval;
}

uint32_t adfs_dir_retr_size(adfs_fs* fs, uint32_t id)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || id > (fs->bnodes_bitmap_size*8-1)) return 0;
	#endif

	uint32_t dirsize = 0;

	adfs_bnode* node = adfs_bnode_read(fs, id); ///[MALLOC]
	if(node == NULL) return 0;

	int i = 0;
	for(; i < node->size; i++)
	{
		if(!(node->is_a_leaf)) dirsize += adfs_dir_retr_size(fs, node->pointer[i]);

		dirsize++;
	}

	if(!(node->is_a_leaf)) dirsize += adfs_dir_retr_size(fs, node->pointer[i]);

	free(node); ///[FREE]
	return dirsize;
}

adfs_entry* adfs_dir_retrieve(adfs_fs* fs, uint32_t id, adfs_entry* ptr, uint32_t* number_of_files)
{
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || id > (fs->bnodes_bitmap_size*8-1) || ptr == NULL || number_of_files == NULL) return NULL;
	#endif

	adfs_bnode* node = adfs_bnode_read(fs, id);
	if(node == NULL) return NULL;

	int i;
	for(i = 0; i < node->size; i++)
	{
		if(!(node->is_a_leaf)) ptr = adfs_dir_retrieve(fs, node->pointer[i], ptr, number_of_files);

		adfs_entry* element = malloc(sizeof(adfs_entry));
		if(element == NULL)
		{
			//previously allocated data:
			free(node);
			while(ptr != NULL)
			{
				adfs_entry* tmp = ptr->next;
				free(ptr->filepath);
				free(ptr);
				ptr = tmp;
			}

			return NULL;
		}

		element->filepath = malloc(strlen(node->name[i])+1);
		strcpy(element->filepath, node->name[i]);
		element->next = NULL;

		ptr->next = element;
		ptr = element;

		(*number_of_files)++;
	}

	if(!(node->is_a_leaf)) ptr = adfs_dir_retrieve(fs, node->pointer[i], ptr, number_of_files); //last element

	free(node);
	return ptr;
}

uint8_t adfs_dir_rewind(adfs_dir* dir)
{
	if(dir == NULL) return ADFS_FAILURE;
	dir->current = dir->files;
	return ADFS_SUCCESS;
}

uint8_t adfs_dir_rm_rec(adfs_fs* fs, uint32_t bnode_id)
{
	///adfs_dir_remove_recursively
	#ifdef ADFS_DEBUG
	if(fs == NULL || fs->file == -1 || bnode_id > (fs->bnodes_bitmap_size*8-1)) return ADFS_FAILURE;
	#endif

	adfs_bnode* bnode = adfs_bnode_read(fs, bnode_id);
	if(bnode == NULL) return ADFS_FAILURE;

	uint8_t leaf = bnode->is_a_leaf;

	int j = 0;
	for(; j < bnode->size; j++)
	{
		if(ADFS_BNODE_READ_ENTRY_TYPE(bnode->entry_type, j) == ADFS_BNODE_ENTRY_TYPE_DIR)
		{
			if(adfs_dir_rm_rec(fs, bnode->cluster[j]) == ADFS_FAILURE)
			{
				free(bnode);
				return ADFS_FAILURE;
			}
		}
		else
		{
			adfs_inode* inode = adfs_inode_read(fs, bnode->cluster[j]);
			if(inode == NULL)
			{
				free(bnode);
				return ADFS_FAILURE;
			}

			if(adfs_file_remove(fs, bnode->cluster[j], inode) == ADFS_FAILURE)
			{
				free(inode);
				free(bnode);
				return ADFS_FAILURE;
			}
			free(inode);
		}

		if(leaf == 0)
		{
			if(adfs_dir_rm_rec(fs, bnode->pointer[j]) == ADFS_FAILURE)
			{
				free(bnode);
				return ADFS_FAILURE;
			}
		}
	}

	if(leaf == 0)
	{
		if(adfs_dir_rm_rec(fs, bnode->pointer[j]) == ADFS_FAILURE)
		{
			free(bnode);
			return ADFS_FAILURE;
		}
	}

	free(bnode);

	if(adfs_bnode_release(fs, bnode_id) == ADFS_FAILURE) return ADFS_FAILURE;

	///statistics
	fs->superblock.number_of_dirs--;

	return ADFS_SUCCESS;
}

uint32_t adfs_dir_size(adfs_fs* fs, const char* dirpath)
{
	if(fs == NULL || fs->file == -1 || dirpath == NULL) return 0; //seems adequate

	uint32_t* dirsize = (uint32_t*) adfs_dir_execute(fs, dirpath, ADFS_DIR_SIZE_FUNCTION);
	if(dirsize == NULL) return 0; //problems with arguments, probably?
	uint32_t redval = *dirsize;
	free(dirsize);

	return redval;
}
